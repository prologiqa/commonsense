:- module(ontology,[worldlet/1, subWorldletOf/2,
	      emotion/1, emotional/3]).

%:- redefine_system_predicate(number(_)).

%:- use_module(time).
%:- use_module(modal).

:- dynamic model:class/2, model:subClassOf/2,
	model:dataProperty/4, model:objectProperty/10.

:- multifile model:class/2, model:subClassOf/2,
	model:dataProperty/4, model:objectProperty/10.

:- dynamic model:class/3, model:subClassOf/3,
	model:dataProperty/5, model:objectProperty/11.

:- multifile model:class/3, model:subClassOf/3,
	model:dataProperty/5, model:objectProperty/11.


model:superClassOf(SUPER,SUB):-model:subClassOf(SUB,SUPER).

model:objectProperty(winniepooh,hasFriend,humanoid,humanoid,_,_,_,_,_,_,_).
model:objectProperty(winniepooh,hasKeeper,humanoid,humanoid,_,_,_,_,_,_,_).
model:objectProperty(winniepooh,hasFodder,humanoid,_,_,_,_,_,_,_,_).

model:dataProperty(winniepooh,hasSex,humanoid,sex,true).
model:dataProperty(winniepooh,hasEmotion,humanoid,emotion,false).

%to denote an instance is belonging to a class
:- op(400,yfx,#).

:- dynamic subWorldletOf/2,
	residence/1, hut/1,
	land/1, wood/1, emotion/1, emotion/2,
	friend/1, emotional/2,
	fodder/2, fodder/3,
	geopart / 2, nameplate/3, residence/2,
	sex/2, keeper/2, love/4.

:- multifile subWorldletOf/2,
	residence/1, hut/1,
	land/1, wood/1, emotion/1, emotion/2,
	friend/1, emotional/2,
	fodder/2, fodder/3,
	geopart / 2, nameplate/3, residence/2,
	sex/2, keeper/2, love/4.

%wordlet structure
%isWorldlet(root). is coming from OWL
:- dynamic worldlet/1 .

%
% root worldlet - better to say general worldlet
% general information - extension of the ontology
%
%emotion(pedantry).
%emotion(scholasticNature).
%emotion(shyness).
%emotion(touchyness).
%emotion(happyness).
%emotion(bouncingMood).

fodder(rabbit,cabbage).

fodder(owl,mouse).

fodder(bear,honey).

fodder(pig,maize).

%inverse(child,parent).
%child(P,C):- parent(C,P).

:- dynamic
        humanoid/2, adult/2, bear/2, boy/2, child/2, donkey/2,
	female/2, friend/2, human/2, kangaroo/2, male/2, mother/2,
	animal/2, bird/2, owl/2, pig/2, rabbit/2, tiger/2,
	carnivore/2, omnivore/2, herbivore/2, marsupial/2,
	residence/2, hut/2, day/2, month/2, season/2,
	land/2, wood/2,
	friend/2, mother/2,
	emotion/3, fodder/2, fodder/3,
	geopart / 3, residence/3,
	parent/3, sex/3, keeper/3,
	eat/3.

:- multifile be/4,
        humanoid/2, adult/2, bear/2, boy/2, child/2, donkey/2,
	female/2, friend/2, human/2, kangaroo/2, male/2, mother/2,
	animal/2, bird/2, owl/2, pig/2, rabbit/2, tiger/2,
	carnivore/2, omnivore/2, herbivore/2, marsupial/2,
	residence/2, hut/2, day/2, month/2, season/2,
	land/2, wood/2, emotion/2,
	friend/2, mother/2,
	emotion/3, fodder/2, fodder/3,
	geopart / 3, residence/3,
	parent/3, sex/3, keeper/3.



female(W,ANIMAL):- sex(W,ANIMAL,female).
male(W,ANIMAL):- sex(W,ANIMAL,male).

boy(W,X):- child(W,X), sex(W,X,male).

love(W,X,_,WHOM):- friend(W,X,WHOM).

%a child lives together with its mother
live(W,CHILD,HERE):- nonvar(CHILD),
	mother(W,CHILD,MOTHER), once(child(W,CHILD)), !,
	live(W,MOTHER,HERE).


live(W,X,HERE):- residence(W,X,HERE).
%if somebody lives somewhere, then also lives in embedding regions
live(W,WHO,THERE):-
	nonvar(THERE), !, geopart(W,THERE,HERE), live(W,WHO,HERE).

eat(W,X,WHAT):- fodder(W,X,WHAT).
eat(W,X#CLASS,WHAT):- fodder(W,CLASS,WHAT), animal(W,X#CLASS).

eat(W,SUBJ,CLASS,WHAT):- var(WHAT), eat(W,SUBJ,CLASS).

have(W,SUBJ,CLASS,WHAT):- CALL =.. [CLASS,W,SUBJ,WHAT], CALL.

camelConcat(PRE,SUF,CAMEL):- atom_chars(SUF,[S|UF]), upcase_atom(S,SUP),
    atomic_list_concat([PRE,SUP|UF],CAMEL).

follow(_,monday#weekday,weekday,sunday#weekday):- ! .
follow(_,NEXT#weekday,weekday,DAY#weekday):-
  findall(D,weekday(D),DAYS), append(_,[DAY,NEXT|_],DAYS).

follow(_,january#month,month,december#month):- ! .
follow(_,NEXT#month,month,MONTH#month):-
  findall(D,yearmonth(D),MONTHS), append(_,[MONTH,NEXT|_],MONTHS).

come(W,NEXT,after(weekday),DAY):-
  follow(W,NEXT,weekday,DAY).
come(W,NEXT,after(month),MONTH):-
  follow(W,NEXT,month,MONTH).

precede(_,sunday#weekday,weekday,monday#weekday):- ! .
precede(_,PREV#weekday,weekday,DAY#weekday):-
  findall(D,weekday(D),DAYS), append(_,[PREV,DAY|_],DAYS).

precede(_,december#month,month,january#month):- ! .
precede(_,PREV#month,month,MONTH#month):-
  findall(M,yearmonth(M),MONTHS), append(_,[PREV,MONTH|_],MONTHS).

day(X#weekday):- weekday(X).
month(X#month):- yearmonth(X).
season(X#season):- yearseason(X).

geoDistance(budapest#city,vienna#city,280).

geoXDistance(FROM,TO,DIST):- geoDistance(FROM,TO,DIST).
geoXDistance(TO,FROM,DIST):- geoDistance(FROM,TO,DIST).

be(from(_),FROM,TO,DIST):- once(geoXDistance(FROM,TO,DIST)).

be(D#_,day,in(week)):- weekday(D).
be(M#_,month,in(year)):- yearmonth(M).
be(SY,season,in(year)):- yearseason(SY).

be(_,WD,day,in(week)):- be(WD,day,in(week)).
be(_,MY,month,in(year)):- be(MY,month,in(year)).
be(_,SY,season,in(year)):- be(SY,season,in(year)).













