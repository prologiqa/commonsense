:- module(model,[domain/2, range/2, functional/1, inverseFunctional/1
                 ,transitive/1, symmetric/1, antisymmetric/1
                 ,reflexive/1, irreflexive/1
		,subClassOf/2, class/2, datatype/2
            ,subObjectPropertyOf/2, objectProperty/10
            ,subDataPropertyOf/2, dataProperty/4
            ,relation/2, subRelationOf/2
            ,disjoint/1, equivalent/1
                ,domain/3, range/3, functional/2, inverseFunctional/2
                 ,transitive/2, symmetric/2, antisymmetric/2
                 ,reflexive/2, irreflexive/2
		,subClassOf/3, superClassOf/2, class/3, datatype/3
            ,subObjectPropertyOf/3, objectProperty/11
            ,subDataPropertyOf/3, dataProperty/5
            ,relation/3, subRelationOf/3
            ,disjoint/2, equivalent/2

            ,class/1, datatype/1, objectProperty/1, dataProperty/1
            ,property/1, relation/1 ]).

%disjoint(LISTOFGENERALIZABLES)
:- dynamic disjoint/1.
:- dynamic disjoint/2.

%equivalent(LISTOFEQUIVALENTS)
:- dynamic equivalent/1.
:- dynamic equivalent/2.

%class(CLASS,CTYPE)
%CTYPE: class;abstract
:- dynamic class/2.
:- dynamic class/3.

%datatype(TYPE,DEF)
:- dynamic datatype/2.
:- dynamic datatype/3.

% female-SubClassOf-human
%subClassOf(SUB,SUPER).
:- dynamic subClassOf/2.
:- dynamic subClassOf/3.

% objectProperty(PROPERTY,DOMAIN,RANGE,
% FUNC,INVFUNC,TRANS,SYMM,ANTISYMM,REFL,IRREFL).
:- dynamic objectProperty/10.
:- dynamic objectProperty/11.

%dataProperty(PROPERTY,DOMAIN,RANGE,FUNC).
:- dynamic dataProperty/4.
:- dynamic dataProperty/5.

%subObjectPropertyOf(SUB,SUPER).
:- dynamic subObjectPropertyOf/2.
:- dynamic subObjectPropertyOf/3.

%subDataPropertyOf(SUB,SUPER).
:- dynamic subDataPropertyOf/2.
:- dynamic  subDataPropertyOf/3.

%relation(RELATION,DOMAINS).
:- dynamic relation/2.
:- dynamic relation/3.

%subRelationOf(SUB,SUPER).
:- dynamic subRelationOf/2.
:- dynamic subRelationOf/3.


:- include(model0).
/*
class(thing,abstract).
objectProperty(topObjectProperty,_,_,_,_,_,_,_,_,_).
dataProperty(topDataProperty,_,_,_).
relation(topRelation,_).

class(_W,CL,KIND):-class(CL,KIND).
datatype(_W,TYPE,DEF):-datatype(TYPE,DEF).
subClassOf(_W,SUB,SUPER):-subClassOf(SUB,SUPER).
objectProperty(_W,PROP,DOM,RNG,FUN,IFUN,TRANS,SYMM,ASYMM,REFL,IREFL):-
    objectProperty(PROP,DOM,RNG,FUN,IFUN,TRANS,SYMM,ASYMM,REFL,IREFL).
subObjectPropertyOf(_W,SUB,SUPER):-subObjectPropertyOf(SUB,SUPER).
dataProperty(_W,PROP,DOM,RNG,FUN):-dataProperty(PROP,DOM,RNG,FUN).
relation(_W,REL,DOMAINS):-relation(REL,DOMAINS).
subRelationOf(_W,SUB,SUPER):-subRelationOf(SUB,SUPER).
subDataPropertyOf(_W,SUB,SUPER):-subDataPropertyOf(SUB,SUPER).
*/

superClassOf(SUPER,SUB):- subClassOf(SUB,SUPER).

forEachAncestorClass(CLASS,CLASS).
forEachAncestorClass(CLASS,ANCESTOR):- subClassOf(CLASS,SUPER),
    forEachAncestorClass(SUPER,ANCESTOR).

forEachAncestorClass(_W,CLASS,CLASS).
forEachAncestorClass(W,CLASS,ANCESTOR):- subClassOf(W,CLASS,SUPER),
    forEachAncestorClass(W,SUPER,ANCESTOR).

forEachAncestorObjectProperty(PROP,PROP).
forEachAncestorObjectProperty(PROP,ANCESTOR):- subObjectPropertyOf(PROP,SUPER),
    forEachAncestorObjectProperty(SUPER,ANCESTOR).

forEachAncestorObjectProperty(_W,PROP,PROP).
forEachAncestorObjectProperty(W,PROP,ANCESTOR):-
    subObjectPropertyOf(W,PROP,SUPER),
    forEachAncestorObjectProperty(W,SUPER,ANCESTOR).

forEachAncestorDataProperty(PROP,PROP).
forEachAncestorDataProperty(PROP,ANCESTOR):- subDataPropertyOf(PROP,SUPER),
    forEachAncestorDataProperty(SUPER,ANCESTOR).

forEachAncestorDataProperty(_W,PROP,PROP).
forEachAncestorDataProperty(W,PROP,ANCESTOR):-
    subDataPropertyOf(W,PROP,SUPER),
    forEachAncestorDataProperty(W,SUPER,ANCESTOR).

forEachAncestorRelation(REL,REL).
forEachAncestorRelation(REL,ANCESTOR):- subRelationOf(REL,SUPER),
    forEachAncestorRelation(SUPER,ANCESTOR).

forEachAncestorRelation(_W,REL,REL).
forEachAncestorRelation(W,REL,ANCESTOR):- subRelationOf(W,REL,SUPER),
    forEachAncestorRelation(W,SUPER,ANCESTOR).

forEachAncestorProperty(PROP,ANCESTOR):-
    forEachAncestorObjectProperty(PROP,ANCESTOR).
forEachAncestorProperty(PROP,ANCESTOR):-
    forEachAncestorDataProperty(PROP,ANCESTOR).

forEachAncestorProperty(W,PROP,ANCESTOR):-
    forEachAncestorObjectProperty(W,PROP,ANCESTOR).
forEachAncestorProperty(W,PROP,ANCESTOR):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR).

class(CLASS):- class(CLASS,_).
datatype(TYPE):- datatype(TYPE,_).
objectProperty(PROP):- objectProperty(PROP,_,_,_,_,_,_,_,_,_).
dataProperty(PROP):- dataProperty(PROP,_,_,_).
property(PROP):- objectProperty(PROP);dataProperty(PROP).
relation(REL):-relation(REL,_).

domain(PROP,DOMAIN):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
    dataProperty(ANCESTOR,DOMAIN,_,_), nonvar(DOMAIN), !.
domain(PROP,DOMAIN):-
    forEachAncestorObjectProperty(PROP,ANCESTOR),
    objectProperty(ANCESTOR,DOMAIN,_,_,_,_,_,_,_,_), nonvar(DOMAIN), !.

domain(W,PROP,DOMAIN):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
    dataProperty(W,ANCESTOR,DOMAIN,_,_), nonvar(DOMAIN), !.
domain(W,PROP,DOMAIN):-
    forEachAncestorObjectProperty(W,PROP,ANCESTOR),
    objectProperty(W,ANCESTOR,DOMAIN,_,_,_,_,_,_,_,_), nonvar(DOMAIN), !.

range(PROP,RANGE):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
    dataProperty(ANCESTOR,_,RANGE,_), nonvar(RANGE), !.
range(PROP,RANGE):-
    forEachAncestorObjectProperty(PROP,ANCESTOR),
    objectProperty(ANCESTOR,_,RANGE,_,_,_,_,_,_,_), nonvar(RANGE), !.

range(W,PROP,RANGE):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
    dataProperty(W,ANCESTOR,_,RANGE,_), nonvar(RANGE), !.
range(W,PROP,RANGE):-
    forEachAncestorObjectProperty(W,PROP,ANCESTOR),
    objectProperty(W,ANCESTOR,_,RANGE,_,_,_,_,_,_,_), nonvar(RANGE), !.

functional(PROP):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
    dataProperty(ANCESTOR,_,_,FUNC), nonvar(FUNC), !, FUNC==true.
functional(PROP):-
    forEachAncestorObjectProperty(PROP,ANCESTOR),
    objectProperty(ANCESTOR,_,_,FUNC,_,_,_,_,_,_), nonvar(FUNC), !, FUNC==true.

functional(W,PROP):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
    dataProperty(W,ANCESTOR,_,_,FUNC), nonvar(FUNC), !, FUNC==true.
functional(W,PROP):-
    forEachAncestorObjectProperty(W,PROP,ANCESTOR),
    objectProperty(W,ANCESTOR,_,_,FUNC,_,_,_,_,_,_), nonvar(FUNC), !, FUNC==true.

inverseFunctional(PROP):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
    objectProperty(ANCESTOR,_,_,_,INVFUNC,_,_,_,_,_),
      nonvar(INVFUNC), !, INVFUNC==true.
inverseFunctional(W,PROP):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
    objectProperty(W,ANCESTOR,_,_,_,INVFUNC,_,_,_,_,_),
      nonvar(INVFUNC), !, INVFUNC==true.


transitive(PROP):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
    objectProperty(ANCESTOR,_,_,_,_,TRANS,_,_,_,_),
      nonvar(TRANS), !, TRANS==true.
transitive(W,PROP):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
    objectProperty(W,ANCESTOR,_,_,_,_,TRANS,_,_,_,_),
      nonvar(TRANS), !, TRANS==true.


symmetric(PROP):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
      objectProperty(ANCESTOR,_,_,_,_,_,SYMM,_,_,_),
      nonvar(SYMM), !, SYMM==true.
symmetric(W,PROP):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
      objectProperty(W,ANCESTOR,_,_,_,_,_,SYMM,_,_,_),
      nonvar(SYMM), !, SYMM==true.

antisymmetric(PROP):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
      objectProperty(ANCESTOR,_,_,_,_,_,_,ASYMM,_,_),
      nonvar(ASYMM), !, ASYMM==true.
antisymmetric(W,PROP):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
      objectProperty(W,ANCESTOR,_,_,_,_,_,_,ASYMM,_,_),
      nonvar(ASYMM), !, ASYMM==true.

reflexive(PROP):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
      objectProperty(ANCESTOR,_,_,_,_,_,_,_,REFL,_),
      nonvar(REFL), !, REFL==true.
reflexive(W,PROP):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
      objectProperty(W,ANCESTOR,_,_,_,_,_,_,_,REFL,_),
      nonvar(REFL), !, REFL==true.

irreflexive(PROP):-
    forEachAncestorDataProperty(PROP,ANCESTOR),
      objectProperty(ANCESTOR,_,_,_,_,_,_,_,_,IRREFL),
      nonvar(IRREFL), !, IRREFL==true.
irreflexive(W,PROP):-
    forEachAncestorDataProperty(W,PROP,ANCESTOR),
      objectProperty(W,ANCESTOR,_,_,_,_,_,_,_,_,IRREFL),
      nonvar(IRREFL), !, IRREFL==true.


















