:-module(logics,[normCalls/2, normCalls/3
		 ,lca/3, lca/4
                ,trClosed/3]).



% flatten calls, and remove true conditions
% (in case of obvious failure generates a single false.)
%normCalls(+CALLS,-NORMALIZED,-LASTCALL).
%normCalls(+CALLS,-NORMALIZED).
%+CALLS: calls hierarchy (embedded conjunctions)
%-NORMALIZED: normalized to a flat calls sequence
%-LASTCALL: the last item of the sequence (a var)
normCalls((true,CALLS),NORM):- !, normCalls(CALLS,NORM).
normCalls((CALLS,true),NORM):- !, normCalls(CALLS,NORM).
normCalls((CALLS1,CALLS2),NORM):- !,
    normCalls(CALLS1,NORM,L), normCalls(CALLS2,L).
normCalls(CALL,CALL):- ! .

normCalls((CALLS1,CALLS2),NORM,LAST):- !,
    normCalls(CALLS1,NORM,L), normCalls(CALLS2,L,LAST).
normCalls(true,C,C):- ! .
normCalls(CALL,CALL,LAST):- LAST==true, ! .
normCalls(CALL,(CALL,LAST),LAST).


%list2Calls([CALL],CALL) :- !.
%list2Calls([CALL|CALLLS],(CALL,CALLS)):-
%    list2Calls(CALLLS,CALLS).


% trClosed(:CALL,ELEM1,ELEM2).
%:CALL the elementary relation to close transitively
%?ELEM1:first parameter of the transitive relation
%?ELEM2:second parameter of the transitive relation
%
% transitive closure, depth first,
% starting from ELEM2, reaching ELEM1, if ELEM2 is nonvar
% or starting from ELEM1, reaching ELEM2, if ELEM1 is nonvar
%
trClosed(_CALL,ELEM,ELEM).
trClosed(CALL,ELEM1,ELEM2):- nonvar(ELEM2), !, call(CALL,ELEM,ELEM2),
    trClosed(CALL,ELEM1,ELEM).
trClosed(CALL,ELEM1,ELEM2):- nonvar(ELEM1), !, call(CALL,ELEM1,ELEM),
    trClosed(CALL,ELEM,ELEM2).



%selfAncestor(CLASS,CLASS).
%selfAncestor(ANCESTOR,CLASS):-
%    model:subClassOf(CLASS,PARENT), selfAncestor(ANCESTOR,PARENT).
%
% Least/lowest common ancestor over two elements
% a brute force algorithm (O(n2))
% (not even correct in case of DAGs)
%lca(C1,C2,PRED,LCA)
%+C1:    element1
%+C2:    element2
%:PRED:  predicate over that LCA is calculated
%?LCA:   the least common ancestor being calculated
%
lca(C1,C2,PRED,LCA):-
    trClosed(PRED,LCA,C1), once(trClosed(PRED,LCA,C2)).

%lca(LIST,PRED,LCA)
%Least/Lowest common ancestor over N elements
%+LIST:  list of elements
%:PRED:  predicate over that LCA is calculated
%?LCA:   the least common ancestor being calculated
%
lca([LCA],_PRED,LCA):- !.
lca([C1,C2|TAIL],PRED,LCA):-
    lca(C1,C2,PRED,LCA0), lca([LCA0|TAIL],PRED,LCA).



