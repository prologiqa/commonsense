%
%
%
%

class(thing,abstract).
objectProperty(topObjectProperty,_,_,_,_,_,_,_,_,_).
dataProperty(topDataProperty,_,_,_).
relation(topRelation,_).

class(_W,CL,KIND):-class(CL,KIND).
datatype(_W,TYPE,DEF):-datatype(TYPE,DEF).
subClassOf(_W,SUB,SUPER):-subClassOf(SUB,SUPER).
objectProperty(_W,PROP,DOM,RNG,FUN,IFUN,TRANS,SYMM,ASYMM,REFL,IREFL):-
    objectProperty(PROP,DOM,RNG,FUN,IFUN,TRANS,SYMM,ASYMM,REFL,IREFL).
subObjectPropertyOf(_W,SUB,SUPER):-subObjectPropertyOf(SUB,SUPER).
dataProperty(_W,PROP,DOM,RNG,FUN):-dataProperty(PROP,DOM,RNG,FUN).
relation(_W,REL,DOMAINS):-relation(REL,DOMAINS).
subRelationOf(_W,SUB,SUPER):-subRelationOf(SUB,SUPER).
subDataPropertyOf(_W,SUB,SUPER):-subDataPropertyOf(SUB,SUPER).

:- dynamic class/2.
:- dynamic objectProperty/10.
:- dynamic dataProperty/4.
:- dynamic relation/2.
