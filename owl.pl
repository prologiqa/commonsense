:- module(owl,[checkAssertOwl/4, rollbackOwl/0]).

:- dynamic owlRef/1.

checkAssertOwl(OWL,W,MODEL,OBJECT):-
  plowl2PL(OWL,W,MCLAUSES,OCLAUSES),
  convlist(checkAssert(MODEL),MCLAUSES,MREFS),
  forall(member(REF,MREFS),assert(owlRef(REF))),
  convlist(checkAssert(OBJECT),OCLAUSES,OREFS),
  forall(member(REF,OREFS),assert(owlRef(REF))).

checkAssert(MOD,HEAD:-BODY,REF):- !,
  \+ clause(MOD:HEAD,BODY), assert(MOD:HEAD:-MOD:BODY,REF).
checkAssert(MOD,FACT,REF):- !,
  \+ clause(MOD:FACT,true), assert(MOD:FACT,REF).

rollbackOwl:-
  retract(owlRef(REF)), erase(REF), fail; true.

plowl2PL(CDEF:CREF,W,[subClassOf(W,CDEF,CREF),class(W,CDEF)],
                       [CRW:-CDW]):-
  atom(CDEF), atom(CREF), CDW=..[CDEF,W,X], CRW=..[CREF,W,X].
plowl2PL(SUBJ:FACT,W,[],[WFACT]):-
  FACT =.. [REL|ARGS], WFACT=.. [REL,W,SUBJ|ARGS].



