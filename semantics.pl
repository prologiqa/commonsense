:- module(semantics, [
          statCheck/2
          ,checkAssert/6
          ,embedResultInSentence/5
          ,sentence2Stmt/5
          ,query2Conjs/8
          ,rollBack/0
          ]).


% semantic operations
% 1. Static semantic checks (not implemented yet)
% 2. Transforming syntax trees to logic expressions
% - Interrogatives are translated to conjunctive queries
% - declaratives are checked: eventually the ontology
%   will be extended by them
% - imperatives are translated to directly executable queries
% 2. Generating answer:
% - true/false for y/n questions
% - wh annotations-->pure values
% - pure values - answer phrases - complete sentences
%
%
% @tbd 1. kijelentő mondatok, eldöntendő mondatok
% (felszólítók)
% @tbd 2. egyéb NP elemek (kvantorok, numerikus elemek, stb.)
%
%:- use_module(enDict).


%
% relations, being converted from verbal expressions
%
% - plain relations, composed of subject and arguments
% SUBJ-V-ARGS ==> VERB(SUBJ|ARGS)
% - relations, composed of subject, object class and object
% SUBJ-V-ARG ==> VERB(SUBJ,CLASS,OBJECT)
% - relations, composed of subject, object class+prefix and object
% SUBJ-V-PRE ARG ==> VERB(SUBJ,PRE(CLASS),OBJECT)
%



%:- use_module(enParse/parser).

%:- use_module(winniePooh/dataface).

:- use_module(owl).

:- use_module(logics).

:- use_module(prolog(dictUtils/prolog/pl2dict)).

:- use_module(library(plunit)).

%denotes assignment
:- op(200,xfy,:=).
%denotes an instance is belonging to a class
:- op(400,yfx,#).


%! staticCheck(TREE,ERRORS) is det.
%
%   Performs static semantic check
%
statCheck(TREE,ERRORS).



% - plain relations, composed of subject and arguments
% SUBJ-V-ARGS ==> VERB(SUBJ|ARGS)
% - relations, composed of subject, object class and object
% SUBJ-V-ARG ==> VERB(SUBJ,CLASS,OBJECT)
% - relations, composed of subject, object class+prefix and object
% SUBJ-V-PRE ARG ==> VERB(SUBJ,PRE(CLASS),OBJECT)
%verbs whose logical arg2 the noun/class of object is injected
%have(piglet#pig,friend,eeyore#donkey).
isInjectArgPrepNounVerb(have,_).
isInjectArgPrepNounVerb(live,_).
isInjectArgPrepNounVerb(eat,_).
isInjectArgPrepNounVerb(love,_).
isInjectArgPrepNounVerb(be,_).
isInjectArgPrepNounVerb(come,after(weekday)).
isInjectArgPrepNounVerb(come,after(month)).
isInjectArgPrepNounVerb(follow,weekday).
isInjectArgPrepNounVerb(precede,weekday).
isInjectArgPrepNounVerb(follow,month).
isInjectArgPrepNounVerb(precede,month).

% - relations, composed of subject, arguments and frees
% SUBJ-V-ARGS-FREES ==> VERB(SUBJ|ARGS|FREES)
isInjectFreePrepNounVerb(live,_).


verbTense(verb{verb:[do|VERBS],tense:TENSE,numpers:_/3},VERB,TENSE,
          verb{verb:VERBS,tense:TENSE,numpers:NUM/3},NUM/3):-
    last(VERBS,VERB), !.
verbTense(verb{verb:VERBS,tense:TENSE,numpers:_/3},VERB,TENSE,
          verb{verb:VERBS,tense:TENSE,numpers:NUM/3},NUM/3):-
    last(VERBS,VERB).


%np2Conjs(+ARGS,-QUANT,-NUMP,-NOUN,-ARGX,-PRED) is det.
%+ARGS: partial syntax tree describing verbal arg
%-QUANT: quantifier
%-NUMP: number/person
%-NOUN: noun, if it is unique
%-ARGX: Prolog object to describe the argument
%-PRED: Prolog call to describe the argument
%
%empty NP
np2Conjs('',_,_,_,_,true):- !.

%'Christopher Robin' ... 'Eeyeore'
np2Conjs(proper{case:_,noun:PROP,class:CLASS,numpers:NUMP},
              inst,NUMP,CLASS,PSEM#CLASS,true):- !,
    enDict:proper_(PROP,_,_,_,CLASS,PSEM).

%'donkey' ==> ?Q,X,donkey(X)
np2Conjs(common{noun:NOUN,case:_,numpers:NUMP},_Q,NUMP,NSEM,ARGX,PRED):-
    enDict:noun_(NOUN,_,_,_,NSEM), PRED=..[NSEM,ARGX], !.

%PLURAL: 'the donkeys' ==> ?QUANT,X,donkey(X)
np2Conjs(art{art:the,numpers:pl/3,kind:def,np:NP},
              univ,pl/3,NOUN,ARGX,PRED):-
    np2Conjs(NP,Q,pl/3,NOUN,ARGX,PRED), var(Q), !.

%SINGULAR: 'the donkey' ==> iota,X,donkey(X)
np2Conjs(art{art:the,numpers:sg/3,kind:def,np:NP},
    iota,sg/3,NOUN,ARGX,PRED):-
    np2Conjs(NP,Q,sg/3,NOUN,ARGX,PRED), var(Q), !.

%'grey donkey' ==> iota,X,grey(X),donkey(X)
np2Conjs(adj{adjp:adj{adj:ADJ,grade:base},grade:base,np:NP},
              Q,NUMP,NOUN,ARGX,(NPPRED,ADJCALL)):-
    enDict:adj_(ADJ,base,_,ASEM), ADJCALL=..[ASEM,ARGX],
    np2Conjs(NP,Q,NUMP,NOUN,ARGX,NPPRED).

%'a donkey' ==> exists,X,donkey(X)
np2Conjs(art{art:a,numpers:sg/3,kind:indef,np:NP},
              Q,NUMP,NOUN,ARGX,PRED):-
    np2Conjs(NP,Q,NUMP,NOUN,ARGX,PRED), !.

%'donkey friend' ==> ?Q,X,donkey(X),friend(X)
np2Conjs(common{noun:[NOUN1,NOUN2]},
              Q,NUMP,NSEM2,ARGX,(PRED2,PRED1)):-
    np2Conjs(common{noun:NOUN1},Q,_,_NSEM1,ARGX,PRED1),
    np2Conjs(common{noun:NOUN2},Q,NUMP,NSEM2,ARGX,PRED2), !.

% 'Eeyore and Kanga' ==>
% [eeyore,kanga],[kangaroo,kangaroo],(true,true)
np2Conjs(coordinating{conn:and,np1:NP1,np2:NP2},
              list,pl/3,[NOUN1,NOUN2],[ARGX1,ARGX2],(PRED1,PRED2)):-
    np2Conjs(NP1,Q,_,NOUN1,ARGX1,PRED1),
    np2Conjs(NP2,Q,_,NOUN2,ARGX2,PRED2).

% 'in Hundred Acre Wood'
np2Conjs(prep{prep:PREP,np:NP},
              Q,NUMP,PREPNOUN,ARGX,ARGCALLS):- !,
    np2Conjs(NP,Q,NUMP,NOUN,ARGX,ARGCALLS), PREPNOUN=..[PREP,NOUN].

% 'Christopher Robin's friends'
%  ==> X,friend(X),have(chrisrobin,friend,X)
np2Conjs(poss{np:NP,numpers:NUM/3,owner:OWNER},
              Q,NUM/3,NOUN,ARGX,ARGCALLS):- !,
    np2Conjs(NP,Q,NUM/3,NOUN,ARGX,NPRED),
    np2Conjs(OWNER,_Q1,_NUMP1,_N,OWARG,OPRED),
    normCalls((OPRED,NPRED),ARGCALLS,have(OWARG,NOUN,ARGX)),
    (NUM=sg->Q=inst;Q=univ).

%'animals living in Hundred Acre Wood' ---- NOT YET READY
np2Conjs(actpart{np:NP,vp:VP},
              Q,NUMP,NOUN,ARGX,CALLS):- !,
    np2Conjs(NP,Q,NUMP,NOUN,ARGX,NPRED),
    vp2Conjs(VP,inst,_PREPNOUN,ARGX,OBJX,_,ARGCALLS,PREDCALL),
    normCalls((NPRED,ARGCALLS),CALLS,PREDCALL).

np2Conjs(INTERR,_,_,_,_,true):- is_dict(INTERR,interr), !.


%which animal (eats maize?)
wh2Conjs(interr{kind:nom,np:NP,numpers:NUM/3,wh:which},
                  _,NUM/3,NOUN,WHX,ARGCALLS):-
    np2Conjs(NP,_,NUM/3,NOUN,WHX,ARGCALLS).

%how many friends (do you have?)
wh2Conjs(interr{kind:num,np:NP,numpers:pl/3,wh:_}, %wh:[how,many]},
                  count,pl/3,NOUN,WHX,ARGCALLS):-
    np2Conjs(NP,_QUANT,pl/3,NOUN,WHX,ARGCALLS).

%what (do you have?)
wh2Conjs(interr{kind:acc,wh:what},
                  inst,_,_,_,true):- !.

%who (eats maize?)
wh2Conjs(interr{kind:nom,wh:who},
                  inst,_,_,_,true):- !.

frees2Conjs([],_,_,_,_,true):- ! .
frees2Conjs([NP],QUANT,NUMP,NOUN,ARGNP,CALLS):- !,
    np2Conjs(NP,QUANT,NUMP,NOUN,ARGNP,CALLS).
frees2Conjs([NP|NPS],QUANT,NUMP,_,[ARGNP|ARGNPS],(CALLS,PREDCALLS)):-
    np2Conjs(NP,QUANT,NUMP,_,ARGNP,CALLS),
    frees2Conjs(NPS,_QUANT,_NUMP,_,[ARGNP|ARGNPS],PREDCALLS).



np2Ans(adj{adjp:adj{adj:A,grade:GRAD},grade:GRAD,np:NP},
       adj{adjp:adj{adj:A,grade:GRAD},grade:GRAD,np:ANSNP},NUMP):-
    np2Ans(NP,ANSNP,NUMP), ! .
np2Ans(common{noun:NOUN,case:CASE,numpers:_},
       common{noun:NOUN,case:CASE,numpers:NUMP},NUMP):-  !.


%how the plurality of a noun phrase changes for to be an answer
npXnpNum(poss{np:NP1,numpers:NUM1/3,owner:OWNER},NUM1,
         poss{np:NP2,numpers:NUM2/3,owner:OWNER},NUM2):-
    npXnpNum(NP1,NUM1,NP2,NUM2).
npXnpNum(adj{adjp:ADJP,grade:G,np:NP},NUM,
         adj{adjp:ADJP,grade:G,np:ANP},ANUM):-
    npXnpNum(NP,NUM,ANP,ANUM).
npXnpNum(prep{prep:PREP,np:NP},NUM,
         prep{prep:PREP,np:ANP},ANUM):-
    npXnpNum(NP,NUM,ANP,ANUM).
npXnpNum(actpart{np:NP,vp:VP},NUM,
         actpart{np:ANP,vp:VP},ANUM):-
    npXnpNum(NP,NUM,ANP,ANUM).
npXnpNum(art{art:ART,numpers:NUM/3,kind:KIND,np:NP},NUM,
         art{art:ART,numpers:ANUM/3,kind:KIND,np:ANP},ANUM):-
    npXnpNum(NP,NUM,ANP,ANUM).
npXnpNum(proper{case:C,noun:PROP,class:CLASS,numpers:NUM/P},NUM,
         proper{case:C,noun:PROP,class:CLASS,numpers:NUM/P},NUM).
npXnpNum(common{case:C,noun:NOUN,numpers:NUM/P},NUM,
         common{case:C,noun:NOUN,numpers:ANUM/P},ANUM).


%nominal clause/verbal phrase - new arg will be injected
nomvp2ArgConjs(vp{verb:VERB0,mode:'',
                       adv:'',tense:TENSE,args:ARGS,free:[]},
            vp{verb:VERB,mode:'',
                       adv:'',tense:TENSE,args:[AARG],free:[]},
                 ARG,QUANT,NUM/P,NOUN,ARGX,CALLS):-
    verbTense(VERB0,be,TENSE,VERB,NUM/P),
    (ARGS='' -> CALLS=true;
    ARGS=[''] -> CALLS=true, AARG=ARG;
    ARGS=[ARG], np2Conjs(ARG,QUANT,_NUMP,NOUN,ARGX,CALLS),
          npXnpNum(ARG,_,AARG,NUM)).

% vp2Conjs/11
% vp2Conjs(+VP,-VPANS,-VAR,-QUANT,?PREPNOUN,
%             -SUBJX,-OBJX,-FREEX,-ARGCALLS,-PREDCALLS) is det.
% +VP:       a dict describing verbal phrase
% -VPANS:  answer VP, with a variable hole
% -VAR:    hole in VPANS - the answer must unified with this
% -QUANT:    quantifier if necessary to unveil
% -PREPNOUN: preposition-noun structure
% -SUBJX:    Prolog object to describe the subject
% -OBJX:     Prolog object to describe the object
% -FREEX:    Prolog object to describe a free argument
% -ARGCALLS: Prolog calls to describe the arguments
% -PREDCALLS:Prolog calls to describe the verb

% subject-no args+freeargs - (How many animals)live in HundredAcreWood?
% free arg and prep/noun is inserted in predicate as params
% answer is generated, as free arg
vp2Conjs11(vp{verb:VERB0,tense:TENSE,adv:_,mode:_,args:[],free:FREES},
         vp{verb:VERB,tense:TENSE,adv:_,mode:_,args:[],free:FREES},
         FREE,QUANT,PREPNOUN,_SUBJXL,NUMP,SUBJX,FREEX,FREECALLS,PREDCALL):- !,
    frees2Conjs(FREES,_,_NUMP,PREPNOUN,FREEX,FREECALLS),
    verbTense(VERB0,VERBSTEM,TENSE,VERB,NUMP),
    (isInjectArgPrepNounVerb(VERBSTEM,PREPNOUN),nonvar(PREPNOUN)->
       PREDCALL=..[VERBSTEM,SUBJX,PREPNOUN,FREEX];
    PREDCALL=..[VERBSTEM,SUBJX,FREEX]).



%(Which animal) eats maize?
%(What) does Piglet eat 'what'?
%(How many friends) does Piglet have?
%VERB-[ARG]-free:[]
vp2Conjs11(vp{verb:VERB0,tense:TENSE,adv:_,mode:_,args:[ARG0],free:FREES},
         vp{verb:VERB,tense:TENSE,adv:_,mode:_,args:ARGS,free:FREES},
         ARG,QUANT,PREPNOUN,SUBJXL,NUMP,SUBJX,ARGX,
           (ARGCALLS,FREECALLS),PREDCALL):- !,
    np2Conjs(ARG0,QUANT,_NUMP,PREPNOUN,ARGX,ARGCALLS),
    frees2Conjs(FREES,_QUANT,_NUMP,PREPNOUN,FREEX,FREECALLS),
    verbTense(VERB0,VERBSTEM,TENSE,VERB,NUMP),
    once(( \+ is_dict(ARG0,interr),ARG=ARG0,ARGS=[ARG];
         ARGS=[ARG])),
    (isInjectArgPrepNounVerb(VERBSTEM,PREPNOUN),nonvar(PREPNOUN)->
       PREDCALL=..[VERBSTEM,SUBJX,PREPNOUN,ARGX];
    PREDCALL=..[VERBSTEM,SUBJX,ARGX]).

% PRED parameters are composed of arguments
% - one extra parameter: PREP(NOUN) is inserted as
%   the second argument of the predicate
%   But only, if it is bound either from outside
%   or from the arguments

%subject+single arg-no freeargs Honnan tudja? have(pooh,friend,X)???
%one single arg is inserted (possibly with prepnoun) in predicate
vp2Conjs(vp{verb:VERB0,tense:TENSE,adv:_,mode:_,args:[NPARG],free:[]},
                QUANT,PREPNOUN,SUBJX,ARGX,_FREEX,ARGCALLS,PREDCALL):- !,
    verbTense(VERB0,VERBSTEM,TENSE,_,_),
    np2Conjs(NPARG,QUANT,NUMP,NOUN,ARGX,ARGCALLS),
    (isInjectArgPrepNounVerb(VERBSTEM,PREPNOUN),nonvar(NOUN)->
      PREDCALL=..[VERBSTEM,SUBJX,NOUN,ARGX];
    PREDCALL=..[VERBSTEM,SUBJX,ARGX]).
%    ignore(arg(1,PREPNOUN,NOUN)),PREDCALL=..[VERB,SUBJX,PREPNOUN,ARGX]).

% subject-no args+freeargs - (How many animals)live in HundredAcreWood?)
% free arg and prep/noun are inserted in predicate as params
vp2Conjs(vp{verb:VERBS, %verb{verb:VERBS,tense:TENSE,numpers:NUM/3},
                       tense:TENSE,adv:_,mode:_,args:[],free:[FREE]},
                QUANT,PREPNOUN,SUBJX,ARGX,_FREEX,ARGCALLS,PREDCALL):- !,
    verbTense(VERBS,VERB,TENSE,_,_),
    np2Conjs(FREE,QUANT,NUMP,PREPNOUN,ARGX,ARGCALLS),
    (isInjectArgPrepNounVerb(VERB,PREPNOUN),nonvar(PREPNOUN)->
       PREDCALL=..[VERB,SUBJX,PREPNOUN,ARGX];
    PREDCALL=..[VERB,SUBJX,ARGX]).

% subject-no args-no free - Where does (Piglet live?)
% simple case: there are no args
% only prep/noun may be inserted in predicate
vp2Conjs(vp{verb:VERBS, %verb{verb:VERBS,tense:TENSE,numpers:NUM/3},
                       tense:TENSE,adv:_,mode:_,args:[],free:[]},
                _QUANT,PREPNOUN,SUBJX,_ARGX,FREEX,true,PREDCALL):-
    verbTense(VERBS,VERB,TENSE,_,_),
    (var(PREPNOUN)-> PREDCALL=..[VERB,SUBJX,FREEX];
    PREDCALL=..[VERB,SUBJX,PREPNOUN,FREEX]).


%clause2PredConjs(+CLAUSE,-ANSWER,-ANSX,-ANSD,-STEM,-NUMP,
%               -QUANT,-WHX,-PRED) is det.
%
%+CLAUSE:dict interrogative clause
%-ANSWER:dict answer clause containing ANSX
%-ANSX:var    part of ANSWER that must be built up later
%-ANSDICT:dict answering phrase, referring to STEM or other
%-STEM:atom   answering dictionary unit
%-NUMP:NUM/PERS of the answering phrase
%-QUANT: quantification designator (empty, each, exist, iota, etc.)
%-WHX: the questioned object, a part of PRED
%:PRED: predicate call to evaluate for the answer (contains WHX)
%
% verbal/wh query-answer

%(Who) is Eeyore? (subject is proper) - which class it belongs to?
%(Who) are Kanga and Baby Roo?
%XXXX(Who) are kangaroos? - which class is its superclass?
%predicative/second order) query - on the model itself
clause2PredConjs(clause{kind:nominal/interrogative(pred),dir:inverted,
                    free:[],subj:SUBJ,vp:VP,numpers:NUMP,pol: +},
             clause{kind:nominal/declarative,dir:straight,
                    free:[],subj:SUBJ,vp:NOMVP,numpers:NUMP,pol: +},
             NOMNP,NOMNPD,STEM,NUMP,QVP,X,CALLS):-
    NOMNPD=common{noun:STEM,case:acc,numpers:NUMP},
    np2Conjs(SUBJ,SQUANT,NUMP,CLASSS,SUBJX,SUBJCALLS), !,
    (SQUANT=list->once(lca(CLASSS,model:superClassOf,CLASS)),STEM=CLASS,
      nomvp2ArgConjs(VP,NOMVP,NOMNP,QVP,NUMP,_PNOUN,SUBJX,PREDCALL),
      X=CLASS,normCalls(SUBJCALLS,CALLS,PREDCALL);
    (var(SUBJX)->STEM=CLASSS,
      nomvp2ArgConjs(VP,NOMVP,NOMNP,QVP,NUMP,_PNOUN,SUBJX,PREDCALL),
      X=SUBJX, normCalls(SUBJCALLS,CALLS,PREDCALL);
    SUBJX=PROP#CLASSS, nonvar(PROP)->
      nomvp2ArgConjs(VP,NOMVP,NOMNP,QVP,sg/3,_PNOUN,SUBJX,PREDCALL),
      X=CLASSS, normCalls(SUBJCALLS,CALLS,PREDCALL))).


whClause2PredConjs(interr{kind:nom,wh:who},
                     CLAUSE,ANSWER,SUBJ,ANSD,STEM,NUMP,WHX,WHX:=QUANTCALLS):- !,
    clause2PredConjs(CLAUSE,ANSWER,SUBJ,ANSD,STEM,NUMP,QUANT,X,CALLS),
     (QUANT==iota-> QUANTCALLS=only(X^CALLS);
     QUANT==univ-> QUANTCALLS=each(X^CALLS);
     QUANTCALLS=X^CALLS).


%whClause2SubjConjs(+IRDICT,+QDICT,-ADICT,-ANSX,-ANSD,
%                   -STEM,-PREPNOUN,-NUMP,-WHX,-PRED) is det.
%+IRDICT:dict syntax tree of the interrogative phrase
%+QDICT:dict  syntax tree of the (interrogative) clause
%-ADICT:dict  syntax tree of the complete answer, referring to ANSX
%-ANSX:var    a var that is part of ADICT
%-ANSD:dict   syntax tree the answer phrase
%-STEM:atom   grammatical stem of the answer as a word
%-PREPNOUN:expr PREP(NOUN) of a noun phrase questioned
%-NUMP:NUM/PERS
%-WHX:expr    the pure answer to the question (if any)
%-PRED:call   Prolog call to describe the relation

%(How far) is Budapest from Vienna?
%ARG->FREE -- WH->ARG
vp2ConjsTernary(vp{verb:VERB0,tense:TENSE,adv:_,mode:_,args:[ARG0],free:FREES},
         vp{verb:VERB,tense:TENSE,adv:_,mode:_,args:[ARG],free:[ARG0|FREES]},
         ARG,QUANT,_SUBJXL,NUMP,SUBJX,ARGX,X,(ARGCALLS,FREECALLS),PREDCALL):- !,
    np2Conjs(ARG0,QUANT,_NUMP,PREPNOUN,ARGX,ARGCALLS),
    frees2Conjs(FREES,_QUANT,_NUMP,_NOUN,_FREEX,FREECALLS),
    verbTense(VERB0,VERBSTEM,TENSE,VERB,NUMP),
    once(( \+ is_dict(ARG0,interr),ARG=ARG0,ARGS=[ARG];
         ARGS=[ARG])),
    (isInjectArgPrepNounVerb(VERBSTEM,PREPNOUN),nonvar(PREPNOUN)->
       PREDCALL=..[VERBSTEM,PREPNOUN,SUBJX,ARGX,X];
    PREDCALL=..[VERBSTEM,SUBJX,ARGX,X]).

:- discontiguous clause2SubjConjs/10.

%(How far) is Budapest from Vienna?
% a nominal subject query - the answer is a measurement
% it is a parser problem - it's rather a predicative query
clause2SubjConjs(clause{kind:nominal/interrogative(wh),dir:inverted,
                    free:[],subj:SUBJ,vp:VP,numpers:NUM/3,pol: +},
             clause{kind:nominal/declarative,dir:straight,
                    free:[],subj:SUBJ,vp:ANSVP,numpers:NUM/3,pol: +},ANSX,
             ANSX,STEM,_,NUM/3,QVP,X,CALLS):- !,
    np2Conjs(SUBJ,_QUANT,NUM/3,_SNOUN,SUBJX,SUBJCALLS),
      vp2ConjsTernary(VP,ANSVP,ANSX,QVP,_SUBJXL,NUM/3,
                 SUBJX,ARGX,X,ARGCALLS,PREDCALL),
      normCalls((SUBJCALLS,ARGCALLS),CALLS,PREDCALL).


%(Who) is donkey? (Who) is the donkey?
%(Who) are the kangaroos? - wh question/ instances class kangaroo have
%(Who) are kangaroos?
%subject queries are straight
%
% a nominal subject query - the answer is one or more proper names
clause2SubjConjs(clause{kind:nominal/interrogative(wh),dir:straight,
                    free:[],subj:SUBJ,vp:VP,numpers:_NUM/3,pol: +},
             clause{kind:nominal/declarative,dir:straight,
                    free:[],subj:ANSX,vp:NOMVP,numpers:ANUM/3,pol: +},ANSX,
             proper{case:nom,noun:STEM,class:CLASS,numpers:sg/3},
                 STEM,_,ANUM/3,QVP,SUBJX,CALLS):- !,
    np2Conjs(SUBJ,_QUANT,_,_SNOUN,SUBJX,SUBJCALLS),
      nomvp2ArgConjs(VP,NOMVP,_,QVP,ANUM/3,_PNOUN,SUBJX,PREDCALL),
      normCalls(SUBJCALLS,CALLS,PREDCALL).

% Subject question - the answer object is the first param of the
% relation that is derived from the verb.
%Who eats maize?
%Which animal eats maize?
clause2SubjConjs(clause{kind:verbal/interrogative(subj),dir:straight,
                         free:[],subj:SUBJ,vp:VP,numpers:NUM/3,pol: +},
                   clause{kind:verbal/declarative,dir:straight,
                         free:[],subj:ANSX,
                          vp:VPANS,numpers:ANUM/3,pol: +},ANSX,
             ANSD,STEM,PREPNOUN,ANUM/3,QVP,SUBJX,CALLS):- !,
    np2Conjs(SUBJ,_QUANT,NUM/3,_SNOUN,SUBJX,SUBJCALLS),
    vp2Conjs11(VP,VPANS,_,QVP,PREPNOUN,SUBJX,
               ANUM/3,SUBJX,_OBJX,ARGCALLS,PREDCALL),
    normCalls((SUBJCALLS,ARGCALLS),CALLS,PREDCALL).


areThere2Conjs(vp{verb:VERB0,tense:TENSE,adv:_,mode:_,
                  args:[adv{adv:there,kind:place,grade:base}],free:[FREE]},
         vp{verb:VERB,tense:TENSE,adv:_,mode:_,args:[],free:[FREE]},
         NUMP,SNOUN,SUBJX,PREDCALL):-
    np2Conjs(FREE,_Q,_,PREPNOUN,_ARGX,_ARGCALLS),
    verbTense(VERB0,be,TENSE,VERB,NUMP),
       PREDCALL=..[be,SUBJX,SNOUN,PREPNOUN].

% NR:=count(X^weekday(X)).
%How many days are there in a week?
%How many states are there in the US?
clause2SubjConjs(clause{kind:nominal/interrogative(wh),dir:straight,free:[],
                 subj:interr{kind:num,np:NPPART,numpers:pl/3,wh:[how,many]},
                 vp:VP,numpers:pl/3,pol: +},
                 clause{kind:nominal/declarative,dir:opposite,
                 free:[adv{adv:there,kind:place,grade:base}],subj:ANSX,
                          vp:VPANS,numpers:pl/3,pol: +},ANSX,
             ANSD,STEM,PREPNOUN,NUMP,count,SUBJX,CALLS):- !,
    np2Conjs(NPPART,_QUANT,pl/3,SNOUN,SUBJX,_SUBJCALLS),
    areThere2Conjs(VP,VPANS,NUMP,SNOUN,SUBJX,CALLS).

%(Which animal) eats maize?
whClause2SubjConjs(CLAUSE,ANSWER,SUBJ,ANSD,STEM,PREPNOUN,NUMP,
                   WHX,QUANTCALLS):- !,
    clause2SubjConjs(CLAUSE,ANSWER,SUBJ,ANSD,STEM,PREPNOUN,NUMP,QUANT,X,CALLS),
     (QUANT==iota-> QUANTCALLS=only(X^CALLS);
     QUANT==univ-> QUANTCALLS=each(X^CALLS);
     QUANTCALLS=X^CALLS).

%Which animal eats maize?
whClause2SubjConjs(interr{kind:nom,wh:which,np:WHNP,numpers:_},
                   CLAUSE,ANSWER,SUBJ,ANSD,STEM,PREPNOUN,NUMP,
                   WHX,WHX:=QUANTCALLS):- !,
    ANSD=proper{case:nom,noun:STEM,class:CLASS,numpers:sg/3},
    wh2Conjs(interr{kind:nom,wh:which,np:WHNP,numpers:_},
             _,NUMP,_PREPNOUN,WHX,WHCALLS),
    clause2SubjConjs(CLAUSE,ANSWER,SUBJ,ANSD,STEM,PREPNOUN,NUMP,QUANT,
                     WHX,CONJCALLS),
    normCalls(WHCALLS,CALLS,CONJCALLS),
     (QUANT==iota-> QUANTCALLS=only(X^CALLS);
     QUANT==univ-> QUANTCALLS=each(X^CALLS);
     QUANTCALLS=WHX^CALLS).


%Who eats maize? *Who* is the pig?
whClause2SubjConjs(interr{kind:nom,wh:who},
                     CLAUSE,ANSWER,SUBJ,ANSD,STEM,PREPNOUN,
                   NUMP,WHX,WHX:=CALLS):- !,
    ANSD=proper{case:nom,noun:STEM,class:CLASS,numpers:sg/3},
    whClause2SubjConjs(CLAUSE,ANSWER,SUBJ,ANSD,STEM,PREPNOUN,NUMP,WHX,CALLS).

%*How far* is Budapest from Vienna?
whClause2SubjConjs(interr{kind:adv{adv:far,kind:place,grade:base},
                          adv:adv(place),wh:how},
                   CLAUSE,ANSWER,ANSX,ANSD,NUMBER,PREPNOUN,NUMP,
                   WHX,WHX:=QUANTCALLS):- !,
    ANSD=adjP(card,NUMBER,kilometer,far),
    clause2SubjConjs(CLAUSE,ANSWER,ANSX,ANSD,NUMBER,PREPNOUN,NUMP,inst,X,CALLS),
     QUANTCALLS=X^CALLS, WHX=X.

%*How many male friends* live in Hundred Acre Wood?
whClause2SubjConjs(interr{kind:num,wh:[how,many],numpers:pl/3,np:WHNP},
                  CLAUSE,ANSWER,ANSX,ANSD,STEM,PREPNOUN,NUMP,
                   WHX,WHX:=QUANTCALLS):- !,
    ANSD=numeral{kind:card,nr:STEM,numpers:NUMP,np:ANSNP},
    wh2Conjs(interr{kind:nom,wh:which,np:WHNP,numpers:_},
             _,NUMP,_PREPNOUN,X,WHCALLS),
    np2Ans(WHNP,ANSNP,NUMP),
    clause2SubjConjs(CLAUSE,ANSWER,ANSX,ANSNP,STEM,PREPNOUN,NUMP,QUANT,
                     X,CONJCALLS),
    normCalls(WHCALLS,CALLS,CONJCALLS),
     (QUANT==iota-> QUANTCALLS=only(X^CALLS);
     QUANT==list-> QUANTCALLS=each(X^CALLS);
     QUANTCALLS=count(X^CALLS)).



%whClause2ObjConjs(+IRDICT,+QDICT,-ADICT,-ANSX,-ANSD,
%                   -STEM,-NUMP,-WHX,-PRED) is det.
%+IRDICT:dict syntax tree of the interrogative phrase
%+QDICT:dict  syntax tree of the (interrogative) clause
%-ADICT:dict  syntax tree of the complete answer, referring to ANSX
%-ANSX:var    a var that is part of ADICT
%-ANSD:dict   syntax tree the answer phrase
%-STEM:atom   grammatical stem of the answer as a word
%-NUMP:NUM/PERS of the questioned/answered object
%-WHX:expr    the pure answer to the question (if any)
%-PRED:call   Prolog call to describe the relation


%(What) does Eeyore eat? (What) do Piglet and Eeyore eat?
%(How many friends) does Piglet have?
%normal verbal wh clause (object/argument question)
%object/argumens/szabad határozós kérdés => válasz más és más
clause2ObjConjs(clause{kind:verbal/interrogative(wh),dir:inverted,
                    free:[],subj:SUBJ,vp:VP,numpers:NUMP,pol: +},
             clause{kind:verbal/declarative,dir:straight,
                    free:[],subj:SUBJ,vp:VPANS,numpers:NUMP,pol: +},
             ANSX,ANSD,PREPNOUN,ANUMP,SQUANT,X,CALLS):- !,
    np2Conjs(SUBJ,SQUANT,NUMP,CLASSS,SUBJXL,SUBJCALLS),
    (SQUANT=list, is_list(SUBJXL)->
      vp2Conjs11(VP,VPANS,ANSX,_QVP,PREPNOUN,SUBJXL,ANUMP,
                 SUBJX,X,ARGCALLS,PREDCALL),
      normCalls((SUBJCALLS,lists:member(SUBJX,SUBJXL),ARGCALLS),CALLS,PREDCALL);
    SUBJXL=PROP#CLASSS, nonvar(PROP)->
      vp2Conjs11(VP,VPANS,ANSX,_QVP,PREPNOUN,SUBJXL,ANUMP,
                 SUBJXL,X,ARGCALLS,PREDCALL),
      normCalls((SUBJCALLS,ARGCALLS),CALLS,PREDCALL)).

% Object question - the answer object is the second param of the
% relation.
% What does Piglet eat?
whClause2ObjConjs(interr{kind:acc,wh:what},
                  CLAUSE,ANSWER,ANSX,ANSD,STEM,NUMP,WHX,WHX:=QUANTCALLS):- !,
    ANSD=common{case:acc,noun:STEM,numpers:ANUMP},
    clause2ObjConjs(CLAUSE,ANSWER,ANSX,ANSD,STEM,NUMP,QUANT,X,CALLS),
     (QUANT==iota-> QUANTCALLS=only(X^CALLS);
     QUANT==list-> QUANTCALLS=each(X^CALLS);
     QUANTCALLS=X^CALLS).

%How many male friends does Piglet have?
whClause2ObjConjs(interr{kind:num,wh:[how,many],numpers:pl/3,np:WHNP},
                  CLAUSE,ANSWER,ANSX,ANSD,STEM,NUMP,WHXX,WHXX:=QUANTCALLS):- !,
    ANSD=numeral{kind:card,nr:STEM,numpers:NUMP,np:ANSNP},
    wh2Conjs(interr{kind:nom,wh:which,np:WHNP,numpers:_},
             _,NUMP,PREPNOUN,X,WHCALLS),
    np2Ans(WHNP,ANSNP,NUMP),
    clause2ObjConjs(CLAUSE,ANSWER,ANSX,ANSNP,PREPNOUN,NUMP,QUANT,X,CONJCALLS),
    normCalls(WHCALLS,CALLS,CONJCALLS),
    QUANTCALLS=count(X^CALLS).


% subject-no args+freeargs -
% (How many animals)live in HundredAcreWood?
% free arg and prep/noun is inserted in predicate as params
% answer is generated, as free arg
vp2ConjsFree(vp{verb:VERB0,tense:TENSE,adv:_,mode:_,args:[],free:FREES},
         vp{verb:VERB,tense:TENSE,adv:_,mode:_,args:[],free:[FREE|FREES]},
         FREE,PREPNOUN,_SUBJXL,NUMP,
           SUBJX,FREEX,FREECALLS,PREDCALL):- !,
    frees2Conjs(FREES,_,_NUMP,PREPNOUN,FREEX,FREECALLS),
    verbTense(VERB0,VERBSTEM,TENSE,VERB,NUMP),
    (isInjectArgPrepNounVerb(VERBSTEM,PREPNOUN),nonvar(PREPNOUN)->
       PREDCALL=..[VERBSTEM,SUBJX,PREPNOUN,FREEX];
    PREDCALL=..[VERBSTEM,SUBJX,FREEX]).


% clause2FreeConjs(+DICT,-ANS,-ANSX,-QUANT,
% +PREPNOUN,-NUMP,-SUBJX,-WHX,-PRED) is det.
% verbal/wh query (where)
% - the queried param is a relational argument
% - plus evtl one param is PREPNOUN (eg. under(name))
% - but it is a free argument in VP
%+DICT:     dict form of the clause
%-ANS:      dict form of the answer (with empty part:VPFREE)
%-ANSX:     var to contain parameter in dict (now free arg)
%-QUANT:    quantifier
%+PREPNOUN: if the relation should have an extra argument (this)
%-SUBJXL:   the actual subject (semantic label or a list)
%-NUMP:NUM/PERS numpers of the clause
%-SUBJX:    the subject of the sentence in PRED
%-X:        the object of PRED (semantic label)
%-PRED:     predicate call to be evaluated to get X
%
%(where) does Piglet live?
%(where) do Piglet and Eeyore live?
clause2vpFreeConjs(clause{kind:verbal/interrogative(wh),dir:inverted,
                         free:[],subj:SUBJ,vp:VP,numpers:NUMP,pol: +},
                   clause{kind:verbal/declarative,dir:straight,
                         free:[],subj:SUBJ,vp:VPANS,numpers:NUMP,pol: +},
                   VPFREE,QUANT,PREPNOUN,SUBJXL,NUMP,SUBJX,X,CALLS):- !,
    np2Conjs(SUBJ,QUANT,NUMP,_,SUBJXL,SUBJCALLS),
    vp2ConjsFree(VP,VPANS,VPFREE,PREPNOUN,
               SUBJXL,NUMP,SUBJX,X,ARGCALLS,PREDCALL),
      normCalls((SUBJCALLS,ARGCALLS),CALLS,PREDCALL).

%
% The answer is the last parameter of the relation, and
% it is injected as a free argument in the sentence.
%
%Where does Piglet /and Kanga/ live?
%==>Hundred Acre Wood
%==>in the Hundred Acre Wood
%==>They live in the Hundred Acre Wood NOT YET IMPLEMENTED!!!
%==>Piglet /and Kanga/ live in the Hundred Acre Wood
whClause2FreeConjs(interr{kind:adv(place),wh:where},
                   CLAUSE,ANSWER,ANSX,ANSD,STEM,NUMP,WHX,CALLS):-
    ANSD=prep{prep:in,
                np:proper{case:acc,noun:STEM,class:CLASS,numpers:sg/3}},
    clause2vpFreeConjs(CLAUSE,ANSWER,ANSX,
                       QUANT,in(noun),SUBJXL,NUMP,SUBJX,X,CONJCALLS),
    (QUANT==list->
      CALLS=(WHX:=list(X^(lists:member(SUBJX,SUBJXL),CONJCALLS),geopart));
    QUANT==inst-> WHX=X,SUBJX=SUBJXL,CALLS=(WHX:=WHX^CONJCALLS)).

%Under what name does Piglet live?
whClause2FreeConjs(interr{kind:KIND,wh:what,np:WHNP,numpers:sg/3},
                   CLAUSE,ANSWER,ANSX,ANSD,STEM,NUMP,WHX,WHX:=WHX^CALLS):-
    KIND=..[PREP,acc],!,
    ANSD=prep{np:poss{np:art{art:the,kind:def,
                               np:common{noun:name,case:acc,numpers:sg/3}},
                        numpers:sg/3,
                        owner:proper{case:acc,class:_,noun:STEM,numpers:sg/3}},
                prep:PREP},
    np2Conjs(WHNP,_Q,_,WNOUN,WHX,WHCALLS), PREPNOUN=..[PREP,WNOUN],
    clause2vpFreeConjs(CLAUSE,ANSWER,ANSX,_QUANT,PREPNOUN,
                       SUBJX,NUMP,SUBJX,WHX,CONJCALLS),
    normCalls(CONJCALLS,CALLS,WHCALLS).


nomvp2ArgClassConjs(vp{verb:VERB0,mode:'',
                       adv:'',tense:TENSE,args:ARGS,free:[]},
            vp{verb:VERB,mode:'',
                       adv:'',tense:TENSE,args:[ARG],free:[]},
                 ARG,QUANT,NUMP,NOUN,ARGX,CALLS):-
    verbTense(VERB0,be,TENSE,VERB,NUMP),
    (ARGS='' -> CALLS=true;
    ARGS=[''] -> CALLS=true;
    ARGS=[ARG], np2Conjs(ARG,QUANT,_NUMP,NOUN,ARGX,CALLS)).


%ANSX,WHDICT,STEM,NUMPERS,X
%var, interj(yes/no), yes/no, NUMP, +/-
ynNominalClause2Conjs(clause{kind:nominal/interrogative(yn),dir:inverted,
                    free:[],subj:SUBJ,vp:VP,numpers:NUMP,pol: +},
             compound{interj:',',clause1:IJ,
                    clause2:clause{kind:nominal/declarative,dir:straight,
                    free:[],subj:SUBJ,vp:VP,numpers:NUMP,pol: RESULT}},
             IJ,interj{interj:INTERJ},INTERJ,NUMP,RESULT,?(CALLS)):-
    np2Conjs(SUBJ,QUANT,NUMP,_,SUBJXL,SUBJCALLS),
    (QUANT==list->
      nomvp2ArgClassConjs(VP,VP,_ARG,_QUANT,NUMP,NOUN,SUBJX,NOMCALLS),
      CALLS=foreach( (lists:member(SUBJX,SUBJXL),SUBJCALLS),NOMCALLS);
    nomvp2ArgClassConjs(VP,VP,_ARG,_QUANT,NUMP,NOUN,SUBJXL,NOMCALLS),
      normCalls((NOMCALLS,SUBJCALLS),CALLS,true)).

ynVerbalClause2Conjs(clause{kind:verbal/interrogative(yn),dir:inverted,
                    free:[],subj:SUBJ,vp:VP,numpers:NUMP,pol: +},
             compound{interj:',',clause1:IJ,
                    clause2:clause{kind:nominal/declarative,dir:straight,
                    free:[],subj:SUBJ,vp:VP,numpers:NUMP,pol: RESULT}},
             IJ,interj{interj:INTERJ},INTERJ,NUMP,RESULT,?(CALLS)):-
    np2Conjs(SUBJ,_QUANT,NUMP,_SNOUN,SUBJX,SUBJCALLS),
    vp2Conjs11(VP,VP,_,_QVP,_,SUBJX,NUMP,SUBJX,_OBJX,ARGCALLS,PREDCALL),
    normCalls((SUBJCALLS,ARGCALLS),CALLS,PREDCALL).


%query2Conjs(+QDICT,-ADICT,-ANSX,-ANSD,-STEM,-WHX,-PRED) is det.
%+QDICT:dict syntax tree of the query
%-ADICT:dict syntax tree of the complete answer referring to ANSX
%-ANSX:var   a variable that represents the pure answer
%-SDICT:dict the syntax tree the answer that refers to STEM
%-STEM:atom  grammatical stem of the answer as a word
%-WHX:   the pure answer to the question object (if any)
%-PRED:  Prolog call to describe the relation
%
%Classification of english interrogative sentences
%
%wh query
%   wh subject query (straight order)
%      nominal: Who is the president?
%      verbal: Who gave you this book?
%   wh predicative query (inverse order)
%      nominal: Who is Piglet?
%   wh query on bound arg (inverse order)
%      verbal: What does Piglet eat?
%   wh query on free arg (inverse order)
%      nominal: When was it nice?
%      verbal: Under what name does Piglet live?
%
%yn query
%   yn subject query
%      nominal: Is Piglet a shy pig?
%      verbal: Does Piglet eat maize?
%felépítjük itt az in(proper) szerkezetet,
%de lehet, h az mélyebben kellene (pl. in his house, v. far away)
query2Conjs(?{interr:INTERR,clause:CLAUSE},
            ANSWER,ANSX,ANSD,STEM,NUMP,WHX,CALLS):-
%%% subject query: it becomes the first arg of the relation of the verb
  whClause2SubjConjs(INTERR,CLAUSE,ANSWER,ANSX,ANSD,STEM,_,NUMP,WHX,CALLS), !;
%%% predicative nominal query
  whClause2PredConjs(INTERR,CLAUSE,ANSWER,ANSX,ANSD,STEM,NUMP,WHX,CALLS), !;
%%% normal object query: it becomes the second arg of the relation
%%% of the verb
  whClause2ObjConjs(INTERR,CLAUSE,ANSWER,ANSX,ANSD,STEM,NUMP,WHX,CALLS), !;
%%% free argument becomes the new arg of the relation of the verb
  whClause2FreeConjs(INTERR,CLAUSE,ANSWER,ANSX,ANSD,STEM,NUMP,WHX,CALLS), !.

query2Conjs(?{clause:CLAUSE},
            ANSWER,ANSX,INTERJ,IJ,NUMP,RESULT,CALLS):-
  ynNominalClause2Conjs(CLAUSE,ANSWER,ANSX,INTERJ,IJ,NUMP,RESULT,CALLS), !;
  ynVerbalClause2Conjs(CLAUSE,ANSWER,ANSX,INTERJ,IJ,NUMP,RESULT,CALLS), !.

%tegyük fel, hogy egy proper a válasz
%WHX=PROP#CLASS.    %Hey! PROP is a semantic label, not syntactic



% Where do Eeyore and Piglet live? ==>
% quant: lca  (LeastCommonAncestor)
%  setof(X, (lists:member(A,[eeyore,piglet]),live(A,X),XL)...
% Where do Kanga and Roo live? ==>
% the girl reads a book ==> girl(X), book(Y) => olvas(Y,Y).
% ez minden lány minden könyvet olvas!!!
% each lecturers have students ==>
% lecturer(X) => szamar(X).
% who is John Doe's boss? ==>
% U^V^human(X), name(X,John,Doe), boss(X,Y), name(Y,U,V).
% since when has John Doe been working at company(C)==>
% since(DATE)^employee(X), name(X,John,Doe), employed(X,Y)|Y=env(thisFirm).


%verb2Stmt/9
%verb2Stmt(VERBS,VERB,TENSE,NUMPERS,OPNOUN,OBJX,FPNOUN,FPX,CALL)
%
verb2Stmt(VERBS,VERB,TENSE,NUMPERS,_OPNOUN,_OBJX,FPNOUN,FREEX,CALL):-
    verbTense(VERBS,VERB,TENSE,_,NUMPERS),
    isInjectFreePrepNounVerb(VERB,_), !,
      CALL=..[VERB,FPNOUN,FREEX].
verb2Stmt(VERBS,VERB,TENSE,NUMPERS,OPNOUN,OBJX,_FPNOUN,_FREEX,CALL):-
    verbTense(VERBS,VERB,TENSE,_,NUMPERS),
      isInjectArgPrepNounVerb(VERB,_), !, CALL=..[VERB,OPNOUN,OBJX].
verb2Stmt(VERBS,VERB,TENSE,NUMPERS,_OPNOUN,OBJX,_FPNOUN,_FREEX,CALL):-
    verbTense(VERBS,VERB,TENSE,_,NUMPERS),
      CALL=..[VERB,OBJX].


extractNP(prep{np:NP,prep:PREP},PREPNOUN,X):- !,
    extractNP(NP,NOUN,X), PREPNOUN=..[PREP,NOUN].
extractNP(proper{noun:PROPER,numpers:sg/3,class:CLASS,case:acc},
           CLASS,SEM):-
    proper_(PROPER,_,_,_,CLASS,SEM), !;
    string(PROPER), string_to_atom(PROPER,ATOMSEM),
      downcase_atom(ATOMSEM,SEM).
extractNP(common{noun:CLASS,numpers:sg/3,case:acc},CLASS,_).


extractFree([],_,_):- !.
extractFree([FREE],CLASS,FREEX):-
    extractNP(FREE,CLASS,FREEX).

extractArg([],_,_):- !.
extractArg([ARG],CLASS,ARGX):-
    extractNP(ARG,CLASS,ARGX).

%converts the sentence to OWL statement
%!sentence2Stmt(+DICT,-PROPER,-SUBJ,-OWL)
%DICT:   dict form of the sentence
%PROPER: string, in case of new proper names
%SUBJ:   the subject of the statement as a var, or a const
%OWL:    OWL statement
%
%Elephants are herbivores. => elephant:herbivore
sentence2Stmt(clause{kind:nominal/declarative,dir:straight,free:[],
                     subj:common{noun:SUBJ,case:nom,numpers:pl/3},
                     vp:vp{adv:'',free:[],mode:'',tense:present,
                         args:[common{noun:PRED,numpers:pl/3,case:acc}],
                         verb:VERB},
                     numpers:pl/3,pol: +},
            _,pl/3,_,SUBJ:PRED):-
    verbTense(VERB,be,present,_,pl/3), !.

%Heffalump is elephant. ==> heffalump#elephant:elephant.
sentence2Stmt(clause{kind:nominal/declarative,dir:straight,free:[],
                     subj:proper{noun:SUBJ,case:nom,class:SCLASS,numpers:sg/3},
                     vp:vp{adv:'',free:[],mode:'',tense:present,
                         args:[common{noun:OCLASS,numpers:sg/3,case:acc}],
                          verb:VERBS},
                     numpers:sg/3,pol: +},
            STRING,sg/3,SEM#SCLASS,SEM#SCLASS:CALL):-
    verbTense(VERBS,be,present,_,sg/3), CALL=OCLASS, !,
    (proper_(SUBJ,_,_,_,SCLASS,SEM)-> true;
    STRING=SUBJ, string_to_atom(SUBJ,ATOMSEM),
      downcase_atom(ATOMSEM,SEM)).
%Piglet eats maize. ==> piglet#pig:eat(maize,_).
%Kanga cooks pancakes. ==> kanga#kangaroo:cook(pancake).
sentence2Stmt(clause{kind:verbal/declarative,dir:straight,free:[],
                     subj:proper{noun:SUBJ,case:nom,class:SCLASS,numpers:sg/3},
                     vp:vp{adv:'',free:FREES,mode:'',tense:present,
                         args:ARGS,verb:VERBS},
                     numpers:sg/3,pol: +},
            STRING,sg/3,SEM#SCLASS,SEM#SCLASS:CALL):-
    extractFree(FREES,FCLASS,FREEX), extractArg(ARGS,ACLASS,ARGX),
    verb2Stmt(VERBS,_VERB,present,sg/3,ACLASS,ARGX,FCLASS,FREEX,CALL),
    (proper_(SUBJ,_,_,_,SCLASS,SEM)-> true;
    STRING=SUBJ, string_to_atom(SUBJ,ATOMSEM),
      downcase_atom(ATOMSEM,SEM)).


rollBack:- enDict:rollbackWords, rollbackOwl.

%checkAssert(+LOG,+STRING,+INST,+W,+M,+O) is semidet.
%LOG:  logical form of the statement
%STRING: string form of a new proper
%INST: the new instance SEM#CLASS
%W:    worldlet id
%M:    model layer as a Prolog module
%O:    ontology/data layer as a Prolog module
checkAssert(LOG,STRING,SEM#CLASS,W,M,O):-
    string(STRING)->string_to_atom(STRING,SUBJ),
      enDict:assertWord(proper(SEM,CLASS,SUBJ));
    checkAssertOwl(LOG,W,M,O).

%a megtalált eredménytől függ a nyelvtani szerkezet!
%proper? In Budapest
%common noun? in forest (poohCorner) a default case
%a structure? in her mothers house
%13? so many friends are there in Hundred Acre Wood
%
%%embedResultInSentence(-ANSX,+WHDICT,-WHDX,-NUMPERS,+WHX)
%ANSX: a variable that is a part of the answer dict
%WHDICT: a dict that answers the question
%        (will be either undified with ANSX,
%        or a list is built from it)
%WHDX:   a variable, part of WHDICT, the grammatical stem/lemma
%        of the answer word
%WHX:    the semantical object that answers the question
embedResultInSentence(ANSX,WHDICT,WHDX,NUM/P,WHLX):-
    is_dict(WHDICT,proper), is_list(WHLX), !,
    (WHLX=[PROP#CLASS]->
      embedResultInSentence(ANSX,WHDICT,WHDX,NUM/P,PROP#CLASS);
    WHLX=[_,_]->NUM=pl, ANSX=coordinating{conn:and,np1:NP1,np:NP2},
      findall(WHDICT,(member(PROP#CLASS,WHLX),
                      enDict:proper_(WHDX,sg,nom,_,CLASS,PROP)),[NP1,NP2]);
      findall(WHDICT,(member(PROP#CLASS,WHLX),
                      enDict:proper_(WHDX,sg,nom,_,CLASS,PROP)),NPL),
        NUM=pl,ANSX=coordinating{conn:and,nps:NPL}).
embedResultInSentence(ANSX,WHDICT,WHDX,NUM/P,WHLX):-
    is_dict(WHDICT,common), is_list(WHLX), !,
    (WHLX=[WHX]->
      embedResultInSentence(ANSX,WHDICT,WHDX,NUM/P,WHX);
    WHLX=[_,_]->NUM=pl, ANSX=coordinating{conn:and,np1:NP1,np:NP2},
      findall(WHDICT,(member(X,WHLX),noun_(WHDX,sg,nom,_,X)),[NP1,NP2]);
      findall(WHDICT,(member(X,WHLX),noun_(WHDX,sg,nom,_,X)),NPL),
        NUM=pl,ANSX=coordinating{conn:and,nps:NPL}).
embedResultInSentence(ANSX,WHDICT,STEM,_/3,WHX):-
    WHDICT=prep{np:NPDICT, prep:_P}, !, ANSX=WHDICT,
    embedResultInSentence(_,NPDICT,STEM,_/3,WHX).
embedResultInSentence(ANSX,WHDICT,STEM,_/3,WHX):-
    WHDICT=poss{np:NPDICT, numpers:_/3,owner:_OWNER}, !, ANSX=WHDICT,
    embedResultInSentence(_,NPDICT,STEM,_/3,WHX).
embedResultInSentence(ANSX,WHDICT,STEM,_,WHX):- integer(WHX),
    WHDICT=adjP(card,STEM,_UNIT,_ADJ), !,
    (enDict:card(STEM,WHX), ANSX=WHDICT; STEM=WHX).
embedResultInSentence(ANSX,WHDICT,STEM,_/3,WHX):- integer(WHX),
    WHDICT=numeral{kind:card,nr:STEM,numpers:NUM/3,np:_}, !,
    enDict:card(STEM,WHX), ANSX=WHDICT, (WHX=1 -> NUM=sg;NUM=pl).
embedResultInSentence(WHDICT,WHDICT,STEM,sg/3,WHX):-
    WHX=(PROP#CLASS), !, proper_(STEM,sg,nom,_WORD,CLASS,PROP);
    is_dict(WHDICT,proper), atom(WHX), !,
      (proper_(STEM,sg,nom,_WORD,CLASS,WHX)->true;
      atom_string(WHX,STEM)).
embedResultInSentence(WHDICT,WHDICT,STEM,NUM/3,WHX):-
    is_dict(WHDICT,common), atom(WHX), !,
    ignore(NUM=sg), noun_(STEM,NUM,nom,_,WHX).
embedResultInSentence(WHDICT,WHDICT,_STEM,pl/3,WHLX):- is_list(WHLX), ! .
embedResultInSentence(WHDICT,WHDICT,STEM,_,WHX):- atom(WHX),
    enDict:word(WHX,STEM),! .
embedResultInSentence(WHDICT,WHDICT,WHX,_,WHX):- string(WHX), ! .


%start(TESTFILE):-
%  logFormTestfile(TESTFILE).



