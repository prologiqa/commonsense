:- module(metaDriven,[load/0, load/3, count/3, checkOnt/2,
                 retract/0, retract/2]).
%
% The module loads OWL files,
% - being created by Protégé 5.x
% - being exported in OWL/XML format
%
% Steps:
% 1. OWL/XML is transformed to PLOWL
%   (PLOWL is an abstract Prolog representation of OWL)
% 2. PLOWL --> PL transformation that takes inferential
%    functions also into account
%
% Created by: Kilián Imre, 04.Jan.2021.
%
%to denote an instance is belonging to a class
:- op(400,yfx,#).


:- set_prolog_flag(encoding,iso_latin_1).

:- use_module(meta).
:- use_module(model).
:- add_import_module(metaDriven,ontology,end).
%:- use_module(ontology).
:- use_module(owl).
:- use_module('../winniepooh/dataface').

%general (library) imports
:- use_module(library(debug)).

%ontology(NAME,FILE,MODEL,DATA).
:- dynamic ontology/4.

%for some reason retract doesnt seem to work
%(maybe because of automatic module-prefixes)
%we use clause/3 instead for searching, and erase/1
/*retractAssert(M:HEAD:-M:BODY):-
    debug(metaDriven,'~w',[M:HEAD:-M:BODY]),
%    retract(M:HEAD:-M:BODYCLAUSE)->assert(M:HEAD:-M:BODY);
    clause(M:HEAD,metaDriven:M:BODY,REF)-> erase(REF),fail;
    assert(M:HEAD:-M:BODY).
retractAssert(M:CLAUSE):-
    debug(metaDriven,'~w',[CLAUSE]),
    (CLAUSE=M:class(CLASS,_,_),
      retract(M:class(CLASS,_,_)), assert(CLAUSE), !;
    retract(CLAUSE),fail; assert(CLAUSE)).
*/
retractAssert(M:HEAD:-BODY):-
    debug(metaDriven,'~w::~w',[M,HEAD:-BODY]),
    @(retract(HEAD:-BODY),M), fail;
    @(assert(HEAD:-BODY),M).
%    clause(M:HEAD,metaDriven:M:BODY,REF)-> erase(REF),fail;
retractAssert(M:FACT):-
    debug(metaDriven,'~w::~w',[M,FACT]),
    @(retract(FACT),M),fail; @(assert(FACT),M).

xml2PL(element(MAIN,[],
	[NLSP1,element(MEMBER,[_=OBJ],[]),NLSP2]),_):-
    empty(NLSP1), empty(NLSP2), xml2NoPL(MAIN,MEMBER,OBJ,LABEL), !,
      debug(metaDriven,'Failed:~w',[LABEL]), fail.
xml2PL(element(MAIN,[_,_=OBJ],[]),_):-
      xml2NoPL(MAIN,_,OBJ,LABEL), !,
      debug(metaDriven,'Failed:~w',[LABEL]), fail.
xml2PL(element(MAIN,[],
	  [NLSP1,element(MEMBER,[_=OBJ],[]),NLSP2]),PLEXPR):-
    empty(NLSP1), empty(NLSP2),
      xml2PLowl(MAIN,MEMBER,OBJ,LABEL,PLEXPR), !,
      debug(metaDriven,'~w',[LABEL]).
xml2PL(element(MAIN,[],
	[NLSP1,element(MEMBER1,[_=OBJ1],[]),
        NLSP2,element(MEMBER2,[_=OBJ2],[]),
	  NLSP3]),PLEXPR):-
    empty(NLSP1), empty(NLSP2), empty(NLSP3),
      xml2PLowl(MAIN,MEMBER1,OBJ1,MEMBER2,OBJ2,LABEL,PLEXPR), !,
      debug(metaDriven,'~w',[LABEL]).
xml2PL(element(MAIN,[],
	[NLSP1,element(MEMBER1,[_=OBJ1],[]),
        NLSP2,element(MEMBER2,[_=OBJ2],[]),
	  NLSP3]),PLEXPR):-
    empty(NLSP1), empty(NLSP2), empty(NLSP3),
      xml2PLowl(MAIN,MEMBER1,OBJ1,MEMBER2,OBJ2,LABEL,PLEXPR), !,
      debug(metaDriven,'~w',[LABEL]).
xml2PL(element(MAIN,[],
	  [NLSP1,element(MEMBER,OBJ,[]),NLSP2]),PLEXPR):-
    empty(NLSP1), empty(NLSP2), xml2PLowl(MAIN,MEMBER,OBJ,LABEL,PLEXPR), !,
      debug(metaDriven,'~w',[LABEL]).
xml2PL(element(MAIN,[],LIST),PLEXPR):-
    xml2PLowls(MAIN,LIST,LABEL,PLEXPR),
        debug(metaDriven,'~w',[LABEL]), ! .
xml2PL(element(MAIN,[],
      [NLSP1,
       element(MEMBER1,[_=DATATYPE],[]),NLSP2,
       element(MEMBER2,[],ELEMENTS),NLSP3]),PLEXPR):-
    empty(NLSP1), empty(NLSP2), empty(NLSP3),
    xml2PLowls(MAIN,MEMBER1,DATATYPE,
               MEMBER2,ELEMENTS,LABEL,PLEXPR),
        debug(metaDriven,'~w',[LABEL]), ! .
xml2PL(element(MAIN,[],[NLSP1,element(MEMBER1,OBJ1,[]),NLSP2,
          element(MEMBER2,OBJ2,[]),NLSP3]),PLEXPR):-
    empty(NLSP1), empty(NLSP2), empty(NLSP3),
      xml2PLowls(MAIN,MEMBER1,OBJ1,MEMBER2,OBJ2,LABEL,PLEXPR), !,
      debug(metaDriven,'~w',[LABEL]).
%xml2PL(_ONT,_XML,_PLEXPR):- throwerror.


empty(LINE):- atom(LINE), normalize_space(atom(''),LINE).


%calling: forEachMetaPostorder(container,MCL)
forEachMetaPostorder(TOPMCLASS,MCLASS):-
    metaClass(TOPMCLASS,MTYPE), MTYPE \= primitive, MTYPE \= struct,
    MTYPE \= enum,
      %the top metaclass is container class
      (MTYPE=class, metaRole(TOPMCLASS,_ROLE,1,MAX,CONTMENT), MAX>1000,
        forEachMetaPostorder(CONTMENT,MCLASS);
      %the top metaclass is an abstract/generalizable class
      metaGeneralization(TOPMCLASS,MSUBCLASS),
        forEachMetaPostorder(MSUBCLASS,MCLASS);
      MTYPE \= abstract, MCLASS=TOPMCLASS).

forEachElementPostorder(TOPMCLASS,MCLASS,MODEL,ELEMENT,PLCALL):-
    forEachMetaPostorder(TOPMCLASS,MCLASS),
    metaClass(MCLASS,_,[ELEMENT|_],PLCALL),MODEL:PLCALL.

redefineSystemPredicates(_,[]):- !.
redefineSystemPredicates(MOD,[NAME/ARITY|TAIL]):-
    functor(HEAD,NAME,ARITY), @(redefine_system_predicate(HEAD),MOD),
    redefineSystemPredicates(MOD,TAIL).

countInstances(DATA,class,ELEMENT,LN):-
    CLASSCALL=..[ELEMENT,_],
      findall(X,clause(DATA:CLASSCALL,_,X),XL), length(XL,LN), !,
      debug(metaDriven,'(~w:~w) -- TOTAL:~w',[DATA,CLASSCALL,LN]).
countInstances(DATA,MCLASS,ELEMENT,LN):-
    metaClassAncestor(nAryRelation,MCLASS), !, NRELCALL=..[ELEMENT,_,_],
      findall(X,clause(DATA:NRELCALL,_,X),XL),length(XL,LN), !,
      debug(metaDriven,'(~w:~w) -- TOTAL:~w',[DATA,NRELCALL,LN]).
countInstances(_DATA,_MCLASS,_ELEMENT,0).

incr_nb(STAT,ARG,NR):- arg(ARG,STAT,VAL0), VAL is VAL0+NR,
    nb_setarg(ARG,STAT,VAL).

%!  count(+CONTAINER,+ONTOLOGY,-STATISTICS) is det.
%Counts the objects in OWL/XML knowledge base (ontology)
%+CONTAINER: container metaclass
%+MODEL: Prolog model-module
%-STATISTICS: s/16
% s(SUBCLASS,SUBOBJPROP,OPINST,SUBDATAPROP,DPINST,SUBREL,RELINST,
% DISJ,EQUI,OBJPROP,DATPROP,REL,PRIM,ENUM,CLASS,CONT)
% SUBCLASS     subclass relations (subclasses)
% SUBOBJPROP   sub object property relations
% OPINST       object property instances
% SUBDATAPROP  sub data property relations
% DPINST       data property instances
% SUBREL       subrelations
% RELINST      relation instances
% DISJ         disjunct set declarations
% EQUI         equivalent set declarations
% OBJPROP      object property declarations
% DATPROP      data property declarations
% REL          relation declarations
% PRIM         primitive datatype declarations
% ENUM         enumeration datatype declarations
% CLASS        class declarations
% CONT         container (ontology) declarations
% eg. count(container,test, S).
count(CONTAINER,ONTOLOGY,STAT):-
    length(ARGS,13), maplist(=(0),ARGS), STAT=..[s|ARGS],
    (metaRole(CONTAINER,_,_,MAX,TOPMCLASS),MAX>1000,
      metaClass(CONTAINER,class,[ONTOLOGY,_,MODEL,DATA],METACLASSCALL),
      METACLASSCALL, %redefineSystemPredicates(DATA,[number/1]),
      countEach(MODEL,DATA,TOPMCLASS,STAT),
    debug(metaDriven,'(~w:~w)==1',[MODEL,METACLASSCALL]), fail;
    debug(metaDriven,
's(SUBCL,SUBOP,OPINS,SUBDP,DPINS,SUBR,RINS,DISJ,EQ,OP,DP,REL,DT,EN,CL,C)',
          [])).

countEach(MODEL,DATA,TOPMCLASS,STAT):-
    forEachElementPostorder(TOPMCLASS,MCLASS,MODEL,ELEMENT,MODELCALL),
      MODEL:MODELCALL, countInstances(DATA,MCLASS,ELEMENT,LN),
      LN1 is LN+1, plOWLStatColumn(MCLASS,ARG), incr_nb(STAT,ARG,LN1),
      debug(metaDriven,'(~w:~w)==~w',[MODEL,MODELCALL,LN1]), fail;true.


checkOnt(CONTAINER,ONTOLOGY):- true.


retractInstances(DATA,class,ELEMENT):-
    CLASSCALL=..[ELEMENT,_], retractall(DATA:CLASSCALL), !,
      debug(metaDriven,'retractall(~w:~w)',[DATA,CLASSCALL]).
retractInstances(DATA,datatype,ELEMENT):-
    !, ENUMCALL=..[ELEMENT,_],
      retractall(DATA:ENUMCALL),
      debug(metaDriven,'retractall(~w:~w)',[DATA,ENUMCALL]).
retractInstances(DATA,MCLASS,ELEMENT):-
    metaClassAncestor(nAryRelation,MCLASS), !, NRELCALL=..[ELEMENT,_,_],
      retractall(DATA:NRELCALL),
      debug(metaDriven,'retractall(~w:~w)',[DATA,NRELCALL]); true.


retract(CONTAINER,ONTOLOGY):-
    metaRole(CONTAINER,_,_,MAX,TOPMCLASS),MAX>1000,
      metaClass(CONTAINER,class,[ONTOLOGY,_,MODEL,DATA],METACLASSCALL),
      METACLASSCALL,
     (forEachElementPostorder(TOPMCLASS,MCLASS,MODEL,ELEMENT,MODELCALL),
        MODEL:MODELCALL,
        retractInstances(DATA,MCLASS,ELEMENT),
        retractall(MODEL:MODELCALL),
        debug(metaDriven,'retractall(~w:~w)',[MODEL,MODELCALL]), fail;
      retract(METACLASSCALL),
      debug(metaDriven,'retractall(~w:~w)',[MODEL,METACLASSCALL]), fail;true).


retract:-
    retract(container,commonsense).


convAssertXML2pl(MODEL,DATA,XML):-
    xml2PL(XML,PLOWL)->
      plowl2PL(MODEL,DATA,PLOWL,FUNC,PLSTMT), dynamic(DATA:FUNC),
      (\+ is_list(PLSTMT)->retractAssert(PLSTMT);
      maplist(retractAssert,PLSTMT));
    true.

%!  load(+FILE,+MODEL,+DATA) is det.
%Loads an external OWL/XML knowledge base (ontology)
%+FILE:  name of the OWL/XML file
%+MODEL: Prolog model-module
%+DATA:  Prolog data-module
%
load(FILE,MODEL,DATA):- load_xml(FILE, [XMLTOTAL], []),
    XMLTOTAL=element('Ontology',NAMESPACES,ELEMENTS),
    %redefineSystemPredicates(DATA,[number/1]),
    lists:append(_,[_=FULLIRI],NAMESPACES), inst2PL(FULLIRI,ONTSTR),
    atom_string(ONTOLOGY,ONTSTR), writeln('Ontology'-ONTOLOGY),
    assert(ontology(ONTOLOGY,FILE,MODEL,DATA)),
    @([commonsense/model0],MODEL),
    foreach((member(XML,ELEMENTS),\+ empty(XML)),
            (convAssertXML2pl(MODEL,DATA,XML)->true;throwerror)
           ).


load:- load('commonsense.owl',model,ontology).

load(FILE):- load(FILE,model,ontology).



:- begin_tests(metaDriven).

test(forEachMetaPostorder,
     all(X==[classGeneralization,objectPropertyGeneralization,
            dataPropertyGeneralization,relationGeneralization,
            disjoints,equivalents,objectProperty,dataProperty,
            relation,primitive,enumeration,class,container])):-
    forEachMetaPostorder(container,X).


test(kangaroos, all(X==[kanga#kangaroo,roo#kangaroo])):-
     ontology:kangaroo(winniepooh,X).

test(thePig):-
    WHO:=only(X^pig(X)), WHO==piglet#pig.

test(eachKangaroos):-
    LIST:=each(X^kangaroo(X)),LIST=[kanga#kangaroo,roo#kangaroo].

test(babyRoosMother):-
    WHO:=X^(mother(X),have(roo#kangaroo,mother,X)),WHO=kanga#kangaroo.

test(pigletsMaleFriends):-
    FRIENDS:=each(X^(friend(X),male(X),have(piglet#pig,friend,X))),
    FRIENDS=[chrisRobin#boy,winnieThePooh#bear,tigger#tiger].

test(isPigletAShyPig):-
    ?((pig(piglet#pig),shy(piglet#pig),true)).

test(isPigletShyAnimal):-
    ?((animal(piglet#pig),shy(piglet#pig),true)).

test(nrOfMaleAnimals):-
    NR:=count(X^(animal(X),male(X))), NR=3.

test(nrOfMaleAnimalsLivingInHundredAcreWood):-
    NR:=count(X^(animal(X),male(X),live(X,in(woodland),
                                        hundredAcreWood#woodland))),NR=3 .

test(nrOfMaleFriendsPigletHas):-
    NR:=count(X^(friend(X),male(X),have(piglet#pig,friend,X))),NR=3 .

test(whichAnimalEatsMaize):-
    A:=A^(animal(A),maize(M),eat(A,maize,M)), A=piglet#pig.

test(whatDoesPigletEat):-
    WH:=WH^eat(piglet#pig,WH), WH=maize.

test(whatDoPigletEeyoreEat):-
    WH:=each(F^(lists:member(A,[piglet#pig,eeyore#donkey]),eat(A,F))),
                   WH=[maize,thistle].

test(whereDoesEeyoreLive):-
    LOC:=LOC^live(eeyore#donkey,in(noun),LOC), LOC=poohsCorner.

test(whereDoPigletEeyoreLive):-
    LOC:=list(L^(lists:member(X,[piglet#pig,eeyore#donkey]),
         live(X,in(noun),L)),geopart), LOC=hundredAcreWood#woodland.

test(underWhatNameDoesPigletLive):-
    N:=(N^(live(piglet#pig,under(name),N),name(N))), N="Trespassers W".

test(doesPigletLoveEeyore):-
    ?(love(piglet#pig,donkey,eeyore#donkey)).

test(howFarIsBudapestFromVienna):-
    X:=X^be(from(city),budapest#city,vienna#city,X), X=280.

test(howManyDaysAreThereInWeek):-
    NR:=count(X^(day(X),be(X,day,in(week)))), NR=7.

test(howManyMonthsAreThereInYear):-
    NR:=count(X^(month(X),be(X,month,in(year)))), NR=12.

test(whichDayComesAfterSunday):-
    N:=N^(day(N),come(N,after(weekday),sunday#weekday)),
    N=monday#weekday.

test(whichDayFollowsSunday):-
    N:=N^(day(N),follow(N,weekday,sunday#weekday)),
    N=monday#weekday.

test(whichDayPrecedesSunday):-
    N:=N^(day(N),precede(N,weekday,sunday#weekday)),
    N=saturday#weekday.

test(whichMonthFollowsDecember):-
    N:=N^(month(N),follow(N,month,december#month)), N=january#month.

test(whichMonthPrecedesJanuary):-
    N:=N^(month(N),precede(N,month,january#month)), N=december#month.

:- end_tests(metaDriven).





















