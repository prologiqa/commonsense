:- module(meta,[
%metameta model
             metaCheck/0, metaAssoc/5
             ,metaClassKind/1, metaPower/2
             ,metaClass/2 , metaClass/4, metaRole/5, metaRole/6
             ,metaGeneralization/2
             ,metaClassAncestor/2, metaClassOffspring/2
              ,plowl2PL/5, plOWLStatColumn/2
%metamodel PLOWL from OWL-XML
             ,inst2PL/2
             ,xml2PLowl/5, xml2PLowl/7
             ,xml2NoPL/4, xml2PLowls/4, xml2PLowls/7
          ]).

:- dynamic    metaClassKind/1, metaPower/2, metaAssoc/5
             ,metaClass/2 ,metaClass/4, metaRole/4, metaRole/5
             ,metaGeneralization/2.
:- discontiguous metaClassKind/1, metaPower/2, metaAssoc/5
             ,metaClass/2 ,metaClass/4, metaRole/4, metaRole/5
             ,metaGeneralization/2.

metaClassKind(abstract).
metaClassKind(primitive).
metaClassKind(struct).
metaClassKind(class).
metaClassKind(classoc).
metaClassKind(enum).

metaClass(string,primitive).
metaClass(integer,primitive).
metaClass(real,primitive).

metaClass(CLASS,TYPE):- metaClass(CLASS,TYPE,_,_).

metaClass(container,class,
          [NAME,FILE,MODEL,DATA],ontology(NAME,FILE,MODEL,DATA)).

metaRole(container,name,1,1,string).
metaRole(container,element,1,10000,element).

metaClass(element,abstract).
metaGeneralization(element,generalization).
metaGeneralization(element,generalizables).
metaGeneralization(element,nAryRelation).
metaGeneralization(element,classifiable).

metaClass(classifiable,abstract).
metaRole(classifiable,name,1,1,string).
metaGeneralization(classifiable,datatype).
metaGeneralization(classifiable,class).

metaClass(class,class,[MCLASS,TYPE],class(MCLASS,TYPE)).

metaClass(datatype,abstract).
metaGeneralization(datatype,primitive).
metaGeneralization(datatype,enumeration).

metaClass(primitive,class,[DTYPE,REF],datatype(DTYPE,REF)).

metaClass(enumeration,class,[ENUM,MEMBERS],datatype(ENUM,MEMBERS)).
metaRole(enumeration,member,1,1000,string).

metaClass(generalizable,abstract).
%meta recoursive role - TO and FROM are always the same
%they are also generalized to the same class
metaRole(generalizable,sub,1,100,generalizable,generalization).
metaRole(generalizable,super,1,100,generalizable,generalization).
metaGeneralization(generalizable,class).
metaGeneralization(generalizable,nAryRelation).


metaClass(nAryRelation,abstract).
metaRole(classifiable,name,1,1,string).
metaGeneralization(nAryRelation,property).
metaGeneralization(nAryRelation,relation).


metaClass(property,abstract).
metaRole(property,functional,1,1,boolean).
metaRole(property,domain,1,1,class).
metaGeneralization(property,objectProperty).
metaGeneralization(property,dataProperty).


metaClass(objectProperty,class,
          [MPROP],objectProperty(MPROP,_,_,_,_,_,_,_,_,_)).

metaRole(objectProperty,inverseFunctional,1,1,boolean).
metaRole(objectProperty,transitive,1,1,boolean).
metaRole(objectProperty,symmetric,1,1,boolean).
metaRole(objectProperty,antisymmetric,1,1,boolean).
metaRole(objectProperty,reflexive,1,1,boolean).
metaRole(objectProperty,irreflexive,1,1,boolean).
metaRole(objectProperty,range,1,1,class).

metaClass(dataProperty,class,[MPROP],dataProperty(MPROP,_,_,_)).
metaRole(dataProperty,range,1,1,datatype).

metaClass(relation,class,[MREL,DOMAINS],relation(MREL,DOMAINS)).

metaClass(generalizables,abstract).
metaGeneralization(generalizables,disjoints).
metaGeneralization(generalizables,equivalents).
metaClass(equivalents,class,LIST,equivalent(LIST)).
metaClass(disjoints,class,LIST,disjoint(LIST)).

metaRole(generalizables,name,1,1,string).
metaPower(generalizable,generalizables).

metaClass(generalization,abstract).
metaGeneralization(generalization,classGeneralization).
metaGeneralization(generalization,propertyGeneralization).
metaGeneralization(generalization,relationGeneralization).

metaClass(relationGeneralization,classoc,
                   [SUB,SUPER],subRelationOf(SUB,SUPER)).
metaClass(classGeneralization,classoc,[SUB,SUPER],subClassOf(SUB,SUPER)).
metaClass(propertyGeneralization,abstract).
metaGeneralization(propertyGeneralization,objectPropertyGeneralization).
metaGeneralization(propertyGeneralization,dataPropertyGeneralization).

metaClass(objectPropertyGeneralization,classoc,
          [SUB,SUPER],subObjectPropertyOf(SUB,SUPER)).
metaClass(dataPropertyGeneralization,classoc,
          [SUB,SUPER],subDataPropertyOf(SUB,SUPER)).


metaClassAncestor(CLASS,CLASS).
metaClassAncestor(ANCESTOR,CLASS):-
    metaGeneralization(SUPER,CLASS), metaClassAncestor(ANCESTOR,SUPER).

metaClassOffspring(CLASS,CLASS).
metaClassOffspring(OFFSPRING,CLASS):-
    metaGeneralization(CLASS,SUB), metaClassOffspring(OFFSPRING,SUB).

metaAssoc(FROM,LABEL,MIN,MAX,FROM):-
    metaClassAncestor(ANCESTOR,FROM), metaRole(ANCESTOR,LABEL,MIN,MAX), !.
metaAssoc(FROM,LABEL,MIN,MAX,TO):-
    metaClassAncestor(ANCESTOR,FROM), metaRole(ANCESTOR,LABEL,MIN,MAX,TO), !.

metaObjectClass(CLASS):- metaClass(CLASS,KIND),
    KIND \= enum, KIND \= primitive, KIND \= struct.




unique(ID,QUANTCALL):-
    setof(ID,QUANTCALL,IDS), length(IDS,IDL),
    bagof(ID,QUANTCALL,IDB), length(IDB,IDL).


metaCheckClassKinds:-
    \+ unique(K,metaClassKind(K)),
      debug(meta,'Duplicate item in Metaclass Kind enumeration.',[]), fail;
    true.

metaCheckClasses:-
    \+ unique(C,K^metaClass(C,K)),
      debug(meta,'Duplicate metaclasses.',[]),fail;
    \+ foreach(metaClass(C,K),metaClassKind(K)),
      debug(meta,'Illegal metaclass kind.',[]), fail;
    true.


metaCheckPowers:-
    \+ unique(CL,POW^metaPower(CL,POW)),
      debug(meta,'Duplicate base class in power declaration.',[]), fail;
    \+ unique(POW,CL^metaPower(CL,POW)),
      debug(meta,'Duplicate power class in power declaration.',[]), fail;
    \+ foreach(metaPower(CL,POW),
               (metaObjectClass(CL), metaObjectClass(POW))),
      debug(meta,'Illegal classes in power declaration.',[]), fail; true.


metaCheckRoles:-
    \+ unique(FROM-NAME-TO,MIN^MAX^metaRole(FROM,NAME,MIN,MAX,TO)),
      debug(meta,'Multiple role declaration.',[]), fail;
    \+ foreach(metaRole(FROM,NAME,MIN,MAX,TO),metaObjectClass(FROM)),
      debug(meta,'Unknown class in role declaration.',[]), fail;
    \+ foreach(metaRole(FROM,NAME,MIN,MAX,TO),metaClass(TO,_)),
      debug(meta,'Unknown target type in role declaration.',[]), fail;
    \+ foreach(metaRole(FROM,NAME,MIN,MAX,TO),atom(NAME)),
      debug(meta,'Role label is not atom.',[]), fail;
    \+ foreach(metaRole(FROM,NAME,MIN,MAX,TO),metaClass(TO,_)),
      debug(meta,'Illegal target type in role declaration.',[]), fail;
    \+ foreach(metaRole(FROM,NAME,MIN,MAX,TO),(0=<MIN, MIN=<MAX)),
      debug(meta,'Multiplicity mismatch in role declaration.',[]), fail;
    true.


metaCheckGeneralizations:-
    \+ unique(SUB-SUPER,metaGeneralization(SUB,SUPER)),
      debug(meta,'Multiple generalization.',[]), fail;
    \+ foreach(metaGeneralization(SUB,SUPER),
            (metaObjectClass(SUB), metaObjectClass(SUB))),
    debug(meta,'Illegal classes in generalization.',[]), fail; true.


metaCheck:- metaCheckClassKinds, metaCheckClasses, metaCheckPowers,
    metaCheckGeneralizations, metaCheckRoles.


xml2PLowl('Declaration','Class',CLASSS,
          'Class'/CLASS,class(CLASS,class)):-
    class2PL(CLASSS,CLASS).
xml2PLowl('Declaration','ObjectProperty',PROPERTY,
          'ObjectProperty'/PROP,
          objectProperty(PROP,_DOMAIN,_RANGE,_FUN,_INVFUN,
                       _TRANS,_SYMM,_ANTISYMM,_REFL,_IRREFL)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('Declaration','DataProperty',PROPERTY,
          'DataProperty'/PROP,
          dataProperty(PROP,_DOMAIN,_RANGE,_FUN)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('Declaration','Datatype',DATATYPE,
          'Datatype'/DTYP,datatype(DTYP,_)):-
    class2PL(DATATYPE,DTYP).
xml2PLowl('FunctionalDataProperty','DataProperty',PROPERTY,
          'Functional'/PROP,
           dataProperty(PROP,_DOMAIN,_RANGE,true)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('FunctionalObjectProperty','ObjectProperty',PROPERTY,
          'Functional'/PROP,
           objectProperty(PROP,_DOMAIN,_RANGE,true,_INVFUN,
                       _TRANS,_SYMM,_ANTISYMM,_REFL,_IRREFL)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('InverseFunctionalObjectProperty','ObjectProperty',PROPERTY,
          'Inverse Functional'/PROP,
           objectProperty(PROP,_DOMAIN,_RANGE,_FUN,true,
                       _TRANS,_SYMM,_ANTISYMM,_REFL,_IRREFL)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('SymmetricObjectProperty','ObjectProperty',PROPERTY,
          'Symmetric'/PROP,
           objectProperty(PROP,_DOMAIN,_RANGE,_FUN,_INVFUN,
                       _TRANS,true,_ANTISYMM,_REFL,_IRREFL)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('AntisymmetricObjectProperty','ObjectProperty',PROPERTY,
          'Antisymmetric'/PROP,
           objectProperty(PROP,_DOMAIN,_RANGE,_FUN,_INVFUN,
                       _TRANS,_SYMM,true,_REFL,_IRREFL)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('TransitiveObjectProperty','ObjectProperty',PROPERTY,
          'Transitive'/PROP,
           objectProperty(PROP,_DOMAIN,_RANGE,_FUN,_INVFUN,
                       true,_SYMM,_ANTISYMM,_REFL,_IRREFL)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('ReflexiveObjectProperty','ObjectProperty',PROPERTY,
          'Reflexive'/PROP,
           objectProperty(PROP,_DOMAIN,_RANGE,_FUN,_INVFUN,
                       _TRANS,_SYMM,_ANTISYMM,true,_IRREFL)):-
    property2PL(PROPERTY,PROP).
xml2PLowl('IrreflexiveObjectProperty','ObjectProperty',PROPERTY,
          'Irreflexive'/PROP,
           objectProperty(PROP,_DOMAIN,_RANGE,_FUN,_INVFUN,
                       _TRANS,_SYMM,_ANTISYMM,_REFL,true)):-
    property2PL(PROPERTY,PROP).


xml2PLowl('ObjectPropertyDomain','ObjectProperty',PROPERTY,'Class',CLASSS,
          'Domain'/PROP=DOMAIN,
           objectProperty(PROP,DOMAIN,_RANGE,_FUN,_INVFUN,
                       _TRANS,_SYMM,_ANTISYMM,_REFL,_IRREFL)):-
    property2PL(PROPERTY,PROP), class2PL(CLASSS,DOMAIN).
xml2PLowl('ObjectPropertyRange','ObjectProperty',PROPERTY,
          'Class',CLASSS,'Range'/PROP=RANGE,
           objectProperty(PROP,_DOMAIN,RANGE,_FUN,_INVFUN,
                       _TRANS,_SYMM,_ANTISYMM,_REFL,_IRREFL)):-
    property2PL(PROPERTY,PROP), class2PL(CLASSS,RANGE).
xml2PLowl('DataPropertyDomain','DataProperty',PROPERTY,'Class',CLASSS,
          'Domain'/PROP=DOMAIN,dataProperty(PROP,DOMAIN,_RANGE,_FUN)):-
    property2PL(PROPERTY,PROP), class2PL(CLASSS,DOMAIN).
xml2PLowl('DataPropertyRange','DataProperty',PROPERTY,'Datatype',CLASSS,
          'Range'/PROP=RANGE,dataProperty(PROP,_DOMAIN,RANGE,_FUN)):-
    property2PL(PROPERTY,PROP), class2PL(CLASSS,RANGE).



xml2PLowl('SubDataPropertyOf','DataProperty',SUBPROP,'DataProperty',SUPERPROP,
          SUB-'SubPropertyOf'-SUPER,subDataPropertyOf(SUB,SUPER)):-
  property2PL(SUBPROP,SUB), property2PL(SUPERPROP,SUPER).
xml2PLowl('SubObjectPropertyOf','ObjectProperty',SUBPROP,
          'ObjectProperty',SUPERPROP,
          SUB-'SubPropertyOf'-SUPER,subObjectPropertyOf(SUB,SUPER)):-
  property2PL(SUBPROP,SUB), property2PL(SUPERPROP,SUPER).
xml2PLowl('SubClassOf','Class',SUBCLASS,'Class',SUPERCLASS,
          SUB-'SubClassOf'-SUPER,subClassOf(SUB,SUPER)):-
    class2PL(SUPERCLASS,SUPER), class2PL(SUBCLASS,SUB).
xml2PLowl('ClassAssertion','Class',CLASSS,'NamedIndividual',INSTT,
          'Instance'/CLASS:INST,INST:CLASS):-
    class2PL(CLASSS,CLASS), inst2PL(INSTT,INST).


xml2PLowls('DatatypeDefinition','Datatype',DATATYPE,'DataOneOf',LIST,
           'Enumeration'/DTYP=VALUES,datatype(DTYP,VALUES)):-
    class2PL(DATATYPE,DTYP), convlist(literal2PL,LIST,VALUES).
xml2PLowls('DatatypeDefinition','Datatype',[_=DATATYPE],'Datatype',[_=REFTYP],
           'Datatype'/DTYP=RTYP,datatype(DTYP,RTYP)):-
    class2PL(DATATYPE,DTYP), class2PL(REFTYP,RTYP).
xml2PLowls('EquivalentClasses','Class',[_=CLASS1],'Class',[_=CLASS2],
           'Equivalent'/CL1=CL2,equivalent([CL1,CL2])):-
    class2PL(CLASS1,CL1), class2PL(CLASS2,CL2).

xml2PLowls('DisjointClasses',LIST,'Disjoints':CLASSES,disjoint(CLASSES)):-
    convlist(xmlElement2Class,LIST,CLASSES), !.


xml2NoPL('Declaration','NamedIndividual',IND,'Individual':I):-
    inst2PL(IND,I).
xml2NoPL('Prefix',_,PRE,'Prefix':P):-
    inst2PL(PRE,P).


xmlElement2Class(element('Class',['IRI'=CLASSS],[]),CLASS):-
   atomic_list_concat([_PRE,CLASS],'#',CLASSS), !.


inst2PL(INSTT,INS):-
   split_string(INSTT,'/','',LIST), LIST \= [_], last(LIST,INST),  !,
   inst2PL(INST,INS).
inst2PL(INSTT,INST):-
   atomic_list_concat([_PRE,INST],'#',INSTT), !.
inst2PL(I,I).

literal2PL(element('Literal',[],[LLIT]),LIT):- sub_atom(LLIT,1,_,0,LIT).

property2PL(PROPERTY,PROP):-
   atomic_list_concat([_PRE,PROP],'#',PROPERTY).
property2PL(PROPERTY,PROP):-
   atomic_list_concat([_PRE,PROP],':',PROPERTY).

class2PL(CLASSS,CLASS):- atomic_list_concat([_PRE,CLS],'#',CLASSS), !,
    decap(CLS,CLASS).
class2PL(CLASSS,CLASS):- atomic_list_concat([_PRE,CLS],':',CLASSS),
    decap(CLS,CLASS).

decap(AATOM,ATOM):- sub_atom(AATOM,0,1,AFTER,CAP),
    sub_atom(AATOM,1,AFTER,0,TAIL), downcase_atom(CAP,DECAP),
    concat(DECAP,TAIL,ATOM).
/*
GENERALLY: worldlet-modelobjects inherit worldless/inmodal objects
- this is defined/hard-coded in "model.pl"
- later inherit from its superworldlet!!!


DECLARATION: (class, objectproperty, dataproperty, relation)
- if W is given     =>insert worldletparameter
- if W is not given =>
- - model: generate an immodal declaration
- -        generate a modal-immodal inheritance clause
- -        (later inherit only from superworldlet)
- - data:  generate a modal-immodal inheritance clause

INSTANTIATION
- if W is given     =>insert worldletparameter
- if W is not given =>generate an immodal fact

GENERALIZATION
- if W is given     => insert worldlet
*/


insertW(EXPR,W,WEXPR):- EXPR=..[NAME|ARGS], WEXPR=..[NAME,W|ARGS].
% If W is given, then we insert it to the result expression
plInsWAssert(W,MOD:PLHEAD:-PLBODY,MOD:PLWHEAD:-PLWBODY):-
    !, insertW(PLHEAD,W,PLWHEAD), insertW(PLBODY,W,PLWBODY).
plInsWAssert(W,MOD:PLSTMT,MOD:PLWSTMT):-
    insertW(PLSTMT,W,PLWSTMT).


plowl2PL(MODEL,DATA,W,PLOWL,FUNC,PLWSTMT):- nonvar(W), !,
    plowl2PL(MODEL,DATA,PLOWL,FUNC,PLSTMT),
    (is_list(PLSTMT)-> maplist(plInsWAssert(W),PLSTMT,PLWSTMT);
     PLSTMT=M:PLSTM, plInsWAssert(W,M:PLSTM,M:PLWSTMT)).
plowl2PL(MODEL,DATA,W,PLOWL,FUNC,PLSTMT):- var(W),
    plowl2PL(MODEL,DATA,PLOWL,FUNC,PLSTMT).

enum2PL(MOD,TYPE,MEMBER,MOD:CALL):- CALL=..[TYPE,MEMBER].

%plowl2PL(MODEL,DATA,PLOWL,DYNFUNC,DATA:PLSTMT):-
%+MODEL:   Prolog module for model
%+DATA:    Prolog module for data
%+PLOWL:   PLOWL clause
%-DYNFUNC: functor of dynamic predicate to be created
%-PL:      Prolog clause (or a list of them)
%
%instances: D-boxes - predicates must have been declared
%TODO: arguments need to be transformed!!
%
plowl2PL(MODEL,DATA,INST:CLASS,[],DATA:PLSTMT):-
    MODEL:class(CLASS), \+ MODEL:class(INST), !, PLSTMT=..[CLASS,INST#CLASS].
plowl2PL(MODEL,DATA,INST:PROPERTYX,[],DATA:PLSTMT):-
    PROPERTYX=..[PROP,X], MODEL:property(PROP), !, PLSTMT=..[PROP,INST,X].
plowl2PL(MODEL,DATA,INST:RELATIONX,[],DATA:PLSTMT):-
    RELATIONX=..[REL|LIST], MODEL:relation(REL), !, PLSTMT=..[REL,INST|LIST].
%
%axioms: T-Boxes
%TODO: inheritance from superworldlet (instead of root)
%
%
%datatype declarations
plowl2PL(MODEL,DATA,datatype(TYPE,DEF),TYPE/1,[MODEL:datatype(TYPE,DEF)
                                         |STMTS]):-
    is_list(DEF), !, maplist(enum2PL(DATA,TYPE),DEF,STMTS); !, STMTS=[] .
%class declarations
plowl2PL(MODEL,DATA,class(CLASS,class),CLASS/1,
         [MODEL:class(CLASS,class),
         (DATA:ROOTHEAD:-MODELESS),
         (DATA:SUBHEAD:- WCALL,SUPCALL)]):- !,
    ROOTHEAD=..[CLASS,root,X], MODELESS=..[CLASS,X],
    SUBHEAD=..[CLASS,SUBW,X], SUPCALL=..[CLASS,SUPW,X],
    WCALL=subWorldletOf(SUBW,SUPW).
%subclasses - they must have been already declared
plowl2PL(MODEL,DATA,SUB:SUPER,FUNC,RESULT):-
    MODEL:class(SUB), MODEL:class(SUPER), !,
    plowl2PL(MODEL,DATA,subClassOf(SUB,SUPER),FUNC,RESULT).
plowl2PL(MODEL,DATA,subClassOf(SUB,SUPER),[],
         [MODEL:subClassOf(SUB,SUPER),(DATA:SUPSTMT:-SUBSTMT)]):-
    MODEL:class(SUB), MODEL:class(SUPER), !,
    SUBSTMT=..[SUB,X], SUPSTMT=..[SUPER,X].

% object property declarations
plowl2PL(MODEL,DATA,
    objectProperty(PROP,DOM,RNG,FUN,IFUN,TRANS,SYMM,ASYMM,REFL,IREFL),
    PROP/2,
    [MODEL:objectProperty(PROP,DOM,RNG,FUN,IFUN,TRANS,SYMM,ASYMM,REFL,IREFL),
     (DATA:ROOTHEAD:-MODELESS)]):- !,
    ROOTHEAD=..[PROP,root,X,Y], MODELESS=..[PROP,X,Y] .
%sub object properties - they must have been already declared
plowl2PL(MODEL,DATA,SUB:SUPER,FUNC,RESULT):-
    MODEL:objectProperty(SUB), MODEL:objectProperty(SUPER), !,
    plowl2PL(MODEL,DATA,subObjectPropertyOf(SUB,SUPER),FUNC,RESULT).
plowl2PL(MODEL,DATA,subObjectPropertyOf(SUB,SUPER),[],
         [MODEL:subObjectPropertyOf(SUB,SUPER),
                                DATA:SUPSTMT:-SUBSTMT]):-
    MODEL:objectProperty(SUB), MODEL:objectProperty(SUPER), !,
      SUBSTMT=..[SUB,X,Y], SUPSTMT=..[SUPER,X,Y].

% data property declarations
plowl2PL(MODEL,DATA,dataProperty(PROP,DOM,RNG,FUN),PROP/2,
    [MODEL:dataProperty(PROP,DOM,RNG,FUN),
     (DATA:ROOTHEAD:-MODELESS)]):- !,
    ROOTHEAD=..[PROP,root,X,Y], MODELESS=..[PROP,X,Y] .
%sub data properties - the must have already been declared
plowl2PL(MODEL,DATA,SUB:SUPER,FUNC,RESULT):-
    MODEL:dataProperty(SUB), MODEL:dataProperty(SUPER), !,
    plowl2PL(MODEL,DATA,subDataPropertyOf(SUB,SUPER),FUNC,RESULT).
plowl2PL(MODEL,DATA,subDataPropertyOf(SUB,SUPER),[],
         [MODEL:subDataPropertyOf(SUB,SUPER),DATA:SUPSTMT:-SUBSTMT]):-
    MODEL:dataProperty(SUB), MODEL:dataProperty(SUPER), !,
      SUBSTMT=..[SUB,X,Y], SUPSTMT=..[SUPER,X,Y].

%relation definitions
plowl2PL(MODEL,DATA,relation(REL,DOMAINS),REL/ARITY,
         [MODEL:relation(REL,DOMAINS),
         (DATA:ROOTHEAD:-MODELESS)]):- !,
    length(DOMAINS,ARITY), length(ARGS,ARITY),
    ROOTHEAD=..[REL,root|ARGS], MODELESS=..[REL|ARGS] .
%relation inheritance
plowl2PL(MODEL,DATA,SUB:SUPER,FUNC,RESULT):-
    MODEL:relation(SUB), MODEL:relation(SUPER), !,
    plowl2PL(MODEL,DATA,subRelationOf(SUB,SUPER),FUNC,RESULT).
plowl2PL(MODEL,DATA,subRelationOf(SUB,SUPER),[],
         [subRelationOf(SUB,SUPER),DATA:SUPSTMT:-SUBSTMT]):-
    MODEL:relation(SUB), MODEL:relation(SUPER), !,
    SUBSTMT=..[SUB|ARGS], SUPSTMT=..[SUPER|ARGS].

plowl2PL(MODEL,_DATA,disjoint(LIST),[],MODEL:disjoint(LIST)).
plowl2PL(MODEL,_DATA,equivalent(LIST),[],MODEL:equivalent(LIST)).





plOWLStatColumn(classGeneralization,1).
plOWLStatColumn(objectPropertyGeneralization,2).
plOWLStatColumn(dataPropertyGeneralization,3).
plOWLStatColumn(relationGeneralization,4).
plOWLStatColumn(disjoints,5).
plOWLStatColumn(equivalents,6).
plOWLStatColumn(objectProperty,7).
plOWLStatColumn(dataProperty,8).
plOWLStatColumn(relation,9).
plOWLStatColumn(primitive,10).
plOWLStatColumn(enumeration,11).
plOWLStatColumn(class,12).
plOWLStatColumn(container,13).


